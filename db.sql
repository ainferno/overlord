drop table IF EXISTS character_spells,character_classes,character_skills,character_stats,class_archetypes,characters,races,users,materials,classes_spells,spells,classes,duration_types,distance_types,timecast_types,timecast_types,timecast_types,damage_types,schools CASCADE;
CREATE TABLE schools (
    id SERIAL PRIMARY KEY,
    name varchar(255) UNIQUE,
    rus varchar(255) UNIQUE
);
CREATE TABLE damage_types (
    id SERIAL PRIMARY KEY,
    name varchar(255) UNIQUE,
    rus varchar(255) UNIQUE
);
CREATE TABLE timecast_types (
    id SERIAL PRIMARY KEY,
    name varchar(255) UNIQUE,
    rus varchar(255) UNIQUE
);
CREATE TABLE distance_types (
    id SERIAL PRIMARY KEY,
    name varchar(255) UNIQUE
);
CREATE TABLE duration_types (
    id SERIAL PRIMARY KEY,
    name varchar(255) UNIQUE
);

CREATE TABLE classes (
    id SERIAL PRIMARY KEY,
    name varchar(255) UNIQUE,
    rus varchar(255) UNIQUE,
    dice int
    spell_stat varchar(255)
);

CREATE TABLE spells (
    id SERIAL PRIMARY KEY,
    name varchar(255) UNIQUE,
    rus varchar(255),
    level INT,
    school INT REFERENCES schools(id),
    ritual BOOLEAN,
    concentration BOOLEAN,
    damage INT,
    distance INT REFERENCES distance_types(id),
    timecast INT REFERENCES timecast_types(id),
    duration INT REFERENCES duration_types(id),
    verbal boolean,
    somatic boolean,
    material int,
    url varchar(255),
    description text,
    upper text,
    timecast_full text,
    duration_full text,
    distance_full text
);

CREATE TABLE classes_spells (
    class INT REFERENCES classes(id),
    spell INT REFERENCES spells(id)
);

CREATE TABLE materials (
    id SERIAL PRIMARY KEY,
    text text UNIQUE,
    expendable BOOLEAN,
    cost int
);

CREATE TABLE users (
    id SERIAL PRIMARY KEY,
    name varchar(255) UNIQUE,
    email varchar(255) UNIQUE,
    password varchar(255)
);

CREATE TABLE races (
    id SERIAL PRIMARY KEY,
	name varchar(255),
	rus varchar(255),
	type varchar(255),
	source varchar(255),
	url varchar(255)
);

CREATE TABLE races_abilities (
    id SERIAL PRIMARY KEY,
    race INT REFERENCES races(id),
	key varchar(255),
	name varchar(255),
	short_name varchar(255),
	value INT
);

CREATE TABLE class_archetypes (
    id SERIAL PRIMARY KEY,
    class INT REFERENCES classes(id),
    name varchar(255),
    rus varchar(255)
);

CREATE TABLE characters (
    id SERIAL PRIMARY KEY,
    "user" int REFERENCES users(id),
    name varchar(255) NOT NULL,
    race INT REFERENCES races(id),
    max_health INT,
    health INT,
    age INT,
    height FLOAT,
    weight FLOAT,
    speed INT,
    proficency_bonus INT,
    status int
);

CREATE TABLE character_stats (
    character INT REFERENCES characters(id),
    constitution INT NOT NULL,
    strength INT NOT NULL,
    dexterity INT NOT NULL,
    charisma INT NOT NULL,
    wizdom INT NOT NULL,
    intelligence INT NOT NULL
);

CREATE TABLE character_skills (
    character INT REFERENCES characters(id),
    athletics INT NOT NULL,
    acrobatics INT NOT NULL,
    sleight_of_hand INT NOT NULL,
    stealth INT NOT NULL,
    arcana INT NOT NULL,
    history INT NOT NULL,
    investigation INT NOT NULL,
    nature INT NOT NULL,
    religion INT NOT NULL,
    animal_handling INT NOT NULL,
    insight INT NOT NULL,
    medicine INT NOT NULL,
    perception INT NOT NULL,
    survival INT NOT NULL,
    deception INT NOT NULL,
    intimidation INT NOT NULL,
    perfomance INT NOT NULL,
    persuation INT NOT NULL
);

CREATE TABLE character_classes (
    character INT REFERENCES characters(id),
    class INT REFERENCES class_archetypes(id),
    level int,
    known_spells int
);

CREATE TABLE character_spells (
	character INT REFERENCES characters(id),
	spell INT REFERENCES spells(id),
	prepared BOOLEAN DEFAULT FALSE
);