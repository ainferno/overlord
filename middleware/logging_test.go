package middleware_test

// import (
// 	"bytes"
// 	"fmt"
// 	"log"
// 	"net/http"
// 	"net/http/httptest"
// 	"os"
// 	"testing"

// 	"github.com/stretchr/testify/assert"

// 	"overlord/middleware"
// )

// func TestLogging(t *testing.T) {
// 	// создаем mockHandler
// 	mockHandler := http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {})
// 	// создаем http.ResponseWriter и http.Request
// 	rw := httptest.NewRecorder()
// 	r := httptest.NewRequest("GET", "/test", bytes.NewBufferString("request body"))

// 	// создаем буфер в памяти для вывода лога
// 	var buf bytes.Buffer
// 	logger := log.New(&buf, "", 0)

// 	// создаем middleware.Logging с нашим собственным логгером
// 	log.SetOutput(logger.Writer())
// 	defer log.SetOutput(os.Stderr)
// 	logging := middleware.Logging

// 	// вызываем middleware с mockHandler
// 	logging(mockHandler).ServeHTTP(rw, r)

// 	// проверяем лог
// 	logOutput := buf.String()
// 	expectedLog := fmt.Sprintf("2023/06/17 13:44:33 from: 192.0.2.1:1234, method: GET, url: /test, body: requestbody, duration: 0 milliseconds\n", time.Now())
// 	assert.Contains(t, logOutput, expectedLog)
// }
