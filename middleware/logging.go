package middleware

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
	"time"
)

func Logging(next http.Handler) http.Handler {
	return http.HandlerFunc(func(rw http.ResponseWriter, r *http.Request) {
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Println("Error reading body", err)
		}
		buf := ioutil.NopCloser(bytes.NewBuffer(body))
		r.Body = buf

		start := time.Now()
		next.ServeHTTP(rw, r)
		msg := fmt.Sprintf(
			"from: %s, method: %s, url: %s, body: %s, duration: %d milliseconds",
			r.RemoteAddr,
			r.Method,
			r.RequestURI,
			strings.ReplaceAll(string(body), " ", ""),
			time.Since(start)/1e6,
		)
		log.Println(msg)
	})
}
