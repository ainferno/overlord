import styled from 'styled-components';
import { Table, Input } from 'antd';

export const { Search } = Input;

export const CustomTable = styled(Table)`
  .ant-table-thead > tr > th {
    font-size: 20px;
  }
  .ant-table-tbody > tr > td {
    font-size: 18px;
  }

  @media (max-width: 480px) {
    .ant-table-thead > tr > th {
      font-size: 16px;
    }
    .ant-table-tbody > tr > td {
      font-size: 14px;
    }
  }

  @media (min-width: 481px) and (max-width: 768px) {
    .ant-table-thead > tr > th {
      font-size: 18px;
    }
    .ant-table-tbody > tr > td {
      font-size: 16px;
    }
  }

  @media (min-width: 769px) and (max-width: 1024px) {
    .ant-table-thead > tr > th {
      font-size: 24px;
    }
    .ant-table-tbody > tr > td {
      font-size: 22px;
    }
  }

  @media (min-width: 1025px) {
    .ant-table-thead > tr > th {
      font-size: 16px;
    }
    .ant-table-tbody > tr > td {
      font-size: 14px;
    }
  }
`;