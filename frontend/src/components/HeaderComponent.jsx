import React, { useState } from 'react';
import { Link, useLocation } from 'react-router-dom';
import { Layout, Menu, Button, Drawer } from 'antd';
import { HomeOutlined, InfoCircleOutlined, LoginOutlined, TableOutlined, UserOutlined, BarsOutlined } from '@ant-design/icons';
import styled, { createGlobalStyle } from 'styled-components';

import { ReactComponent as BurgerMenu1 } from '../assets/svg/BurgerMenu1.svg';

const { Header} = Layout;

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
  }

  .ant-menu-item-selected {
    color: #ffffff !important;
    // background-color: transparent !important;
    border-bottom-color: #fff !important;
  }
  .ant-menu-item-selected::after {
    border-bottom: 2px solid #ffffff !important;
  }
`;

const Logo = styled.div`
  width: 120px;
  height: 31px;
  background: #fff;
  margin: 16px 100px 16px 0;
  // float: left;
`;

const StyledHeader = styled(Header)`
    display: flex;
    align-items: center;
    justify-content: space-between;
    // width: 100%;
    height: 64px;
    

    @media (max-width: 1024px) {
        height: 48px;
      }
`;

const StyledMenu = styled(Menu)`
  flex: 1;
  display: flex;
  justify-content: center;
  background-color: #001529;

  @media (max-width: 1200px) {
    display: none;
  }
`;

const StyledMenuItem = styled(Menu.Item)`
  margin: 0 8px;
  color: #fff;

  .ant-menu-light.ant-menu-horizontal >.ant-menu-item-selected::after {
    color: #fff;
    border-bottom-color: #fff;
  }
`;

const LoginButton = styled(Button)`
  margin-left: auto;

  @media (max-width: 1200px) {
    display: none;
  }
`;

const MobileMenuButton = styled(Button)`
  display: none;

  @media (max-width: 1200px) {
    display: block;
  }
`;

const MobileMenuDrawer = styled(Drawer)`
  .ant-drawer-content-wrapper {
    background-color: rgba(255, 255, 255, 0.9);
  }
`;

function HeaderComponent() {
    const location = useLocation();
    const [isMobileMenuOpen, setMobileMenuOpen] = useState(false);

    const handleMobileMenuOpen = () => {
        setMobileMenuOpen(true);
    };

    const handleMobileMenuClose = () => {
        setMobileMenuOpen(false);
    };
    return (
        <>
            <GlobalStyle />
            <StyledHeader>
                <Logo />
                <StyledMenu mode="horizontal" selectedKeys={[location.pathname]}>
                    <StyledMenuItem key="/" icon={<HomeOutlined />}>
                        <Link to="/">Главная</Link>
                    </StyledMenuItem>
                    <StyledMenuItem key="/acctable" icon={<TableOutlined />}>
                        <Link to="/acctable">Таблица аккаунтов</Link>
                    </StyledMenuItem>
                    <StyledMenuItem key="/spell" icon={<TableOutlined />}>
                        <Link to="/spell">Таблица заклинаний</Link>
                    </StyledMenuItem>
                    <StyledMenuItem key="/characters" icon={<BarsOutlined />}>
                        <Link to="/characters">Мои персонажи</Link>
                    </StyledMenuItem>
                </StyledMenu>
                <LoginButton type="primary" shape="round" icon={<LoginOutlined />}>
                    <Link to="/login">Войти</Link>
                </LoginButton>


                <MobileMenuButton type="text" icon={<BurgerMenu1 />} onClick={handleMobileMenuOpen} />
                <MobileMenuDrawer
                    placement="right"
                    closable={false}
                    onClose={handleMobileMenuClose}
                    open={isMobileMenuOpen}
                    width={200}
                    bodyStyle={{ padding: 0 }}
                >
                    <Menu theme="dark" mode="vertical" defaultSelectedKeys={['home']} onClick={handleMobileMenuClose}>
                        <Menu.Item key="/" icon={<HomeOutlined />}>
                            <Link to="/">Главная</Link>
                        </Menu.Item>
                        <Menu.Item key="/acctable" icon={<TableOutlined />}>
                            <Link to="/acctable">Таблица аккаунтов</Link>
                        </Menu.Item>
                        <Menu.Item key="/spell" icon={<TableOutlined />}>
                            <Link to="/spell">Таблица заклинаний</Link>
                        </Menu.Item>
                        <Menu.Item key="/characters" icon={<BarsOutlined />}>
                            <Link to="/characters">Мои персонажи</Link>
                        </Menu.Item>
                        {/* <Menu.Item key="/about" icon={<InfoCircleOutlined />}>
                        <Link to="/about">О нас</Link>
                    </Menu.Item> */}
                        <Menu.Item key="/login" icon={<LoginOutlined />} style={{ marginLeft: 'auto' }}>
                            <Link to="/login">Login</Link>
                        </Menu.Item>
                    </Menu>
                </MobileMenuDrawer>
            </StyledHeader>
        </>

    )
}

export default HeaderComponent;