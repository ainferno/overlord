import React from 'react';
import { NavLink } from 'react-router-dom';
import styled from 'styled-components';


const CustomNavLink = ({ to, exact, children }) => {
    return (
        <StyledNavLink exact={exact} to={to} activeClassName="active">
            {children}
        </StyledNavLink>
    );
};

export default CustomNavLink;


const StyledNavLink = styled(NavLink)`
font-size: 18px;
line-height: 150%;
font-family: var(--font-lexend);
color: var(--absolute-white);
text-align: left;
text-decoration: none;

&.active {
border-radius: 82px;
background-color: var(--grey-15);
display: flex;
flex-direction: row;
align-items: center;
justify-content: flex-start;
padding: 12px 24px;
box-sizing: border-box;
text-align: left;
font-size: 18px;
color: var(--absolute-white);
font-family: var(--font-exend);
text-decoration: none;
}
`; 