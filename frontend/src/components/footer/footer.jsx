import CustomLink from '../link/link';
import {
    LogoIcon, TextButton6, TextButton7, ButtonsContainer1, Container2,
    Icon2, TextButton10, Button, SubContainer1, Text1, Container3, FooterSectionRoot
} from './footer.style';

import Logo from '../../assets/Logo.svg';
import EmailIcon from '../../assets/EmailIcon.svg';
import LocationIcon from '../../assets/LocationIcon.svg';


const Footer = () => {
    return (
        <FooterSectionRoot>
            <Container2>
                <LogoIcon alt="" src={Logo}/>
                <ButtonsContainer1>
                    <CustomLink to="/">Главная</CustomLink>
                    <CustomLink to="/lobby">Мои лобби</CustomLink>
                    <CustomLink to="/characters">Мои персонажи</CustomLink>
                    <CustomLink to="/aboutus">О нас</CustomLink>
                </ButtonsContainer1>
            </Container2>
            <Container3>
                <SubContainer1>
                    <Button>
                        <Icon2 alt="" src={EmailIcon} />
                        <TextButton10>example.support@gmail.com</TextButton10>
                    </Button>
                    <Button>
                        <Icon2 alt="" src={LocationIcon} />
                        <TextButton10>Russia, Moscow</TextButton10>
                    </Button>
                </SubContainer1>
                <Text1>© 2023 DnDoverlord. All rights reserved.</Text1>
            </Container3>
        </FooterSectionRoot>);
};

export default Footer;
