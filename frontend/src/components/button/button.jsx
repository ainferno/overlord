import React from 'react'
import styled from "styled-components";

function ButtonDefault({text}) {
    return (
        <>
            <ButtonRoot>
                {text}
            </ButtonRoot>
        </>
    )
}

export default ButtonDefault;

const ButtonRoot = styled.div`position: relative;
border-radius: 100px;
background-color: var(--grey-11);
border: 1px solid var(--grey-15);
box-sizing: border-box;
width: 100%;
display: flex;
flex-direction: row;
align-items: center;
justify-content: flex-start;
padding: 18px 24px;
gap: 4px;
text-align: left;
font-size: 18px;
color: var(--absolute-white);
font-family: var(--font-inter);
`;