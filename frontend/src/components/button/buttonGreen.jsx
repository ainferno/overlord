import React from 'react'
import styled from "styled-components";

const ButtonGreen = ({ padding, children }) => {
  return (
    <ButtonRoot padding={padding}>
      <Text1>{children}</Text1>
    </ButtonRoot>
  );
};

export default ButtonGreen;

const Text1 = styled.div`
  position: relative;
  line-height: 150%;
`;

const ButtonRoot = styled.div`
  position: relative;
  border-radius: 82px;
  background-color: var(--green-60);
  width: 100%;
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  padding: ${({ padding }) => padding};
  box-sizing: border-box;
  text-align: left;
  font-size: 18px;
  color: var(--grey-11);
  font-family: var(--font-lexend);
`;