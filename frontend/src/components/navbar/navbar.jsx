import { LogoIcon, ButtonsContainer2, ButtonsContainer3, NavbarRoot } from './navbar.style';
import Logo from '../../assets/navbar/Logo.svg';
import CustomNavLink from '../navlink/navlink';
import ButtonGreen from '../button/buttonGreen';

const Navbar = () => {
    return (
        <NavbarRoot>
            <LogoIcon alt="" src={Logo} />
            <ButtonsContainer2>
                <CustomNavLink exact to="/">
                    Главная
                </CustomNavLink>
                <CustomNavLink exact to="/lobby">
                    Мои лобби
                </CustomNavLink>
                <CustomNavLink exact to="/characters">
                    Мои персонажи
                </CustomNavLink>
                <CustomNavLink exact to="/aboutus">
                    О нас
                </CustomNavLink>
            </ButtonsContainer2>
            <ButtonsContainer3>
                <ButtonGreen padding={'14px 30px'}>
                    Вход
                </ButtonGreen>
            </ButtonsContainer3>
        </NavbarRoot>);
};

export default Navbar;
