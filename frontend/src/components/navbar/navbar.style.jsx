import styled from "styled-components";

export const LogoIcon = styled.img`
position: relative;
width: 155.83px;
height: 40.5px;
z-index: 0;
`;
export const Button = styled.div`
border-radius: var(--br-63xl);
background-color: var(--grey-15);
display: flex;
flex-direction: row;
align-items: center;
justify-content: flex-start;
padding: 12px 24px;
`;
export const ButtonsContainer2 = styled.div`
margin: 0 !important;
position: absolute;
top: calc(50% - 24.5px);
left: calc(50% - 244px);
display: flex;
flex-direction: row;
align-items: center;
justify-content: flex-start;
gap: 26px;
z-index: 1;
`;
export const ButtonsContainer3 = styled.div`
width: 103px;
display: flex;
flex-direction: row;
align-items: center;
justify-content: flex-start;
z-index: 2;
color: var(--grey-11);
`;
export const NavbarRoot = styled.div`
position: relative;
border-radius: 100px;
background-color: var(--grey-11);
border: 1px solid var(--grey-15);
box-sizing: border-box;
width: 100%;
display: flex;
flex-direction: row;
align-items: center;
justify-content: space-between;
padding: 20px 34px;
text-align: left;
font-size: var(--font-size-lg);
color: var(--absolute-white);
font-family: var(--font-lexend);
`;