import React from 'react';
import { Link } from "react-router-dom";
import styled from "styled-components";

const CustomLink = ({ to, children }) => {
    return (
        <LinkStyle to={to}>
            {children}
        </LinkStyle>
    );
};

export default CustomLink;

const LinkStyle = styled(Link)`
font-size: 18px;
line-height: 150%;
font-family: var(--font-lexend);
color: var(--absolute-white);
text-align: left;
text-decoration: none;
`;