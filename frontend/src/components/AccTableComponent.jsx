import React, { useState, useEffect } from 'react';
import { Button, Modal, Form, Input, Table, Space } from 'antd';
import axios from 'axios';

import HeaderComponent from './HeaderComponent';

const AccTableComponent = () => {
  const [modalVisible, setModalVisible] = useState(false);
  const [data, setData] = useState([]);
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [loading, setLoading] = useState(false);
  const [confirmLoading, setConfirmLoading] = useState(false);

  const [form] = Form.useForm();

  // const url = process.env.REACT_APP_ADDR + "/api/users";

  const columns = [
    { title: 'Name', dataIndex: 'name', key: 'name' },
    { title: 'Email', dataIndex: 'email', key: 'email' },
    {
      title: 'Operation',
      dataIndex: 'operation',
      key: 'operation',
      render: (_, record) => (
        <Space size="middle">
          <Button onClick={() => handleDelete(record)}>Delete</Button>
        </Space>
      ),
    },
  ];

  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = async () => {
    try {
      const response = await axios.get('http://dndoverlord.ru:3022/api/users');
      setData(response.data);
    } catch (error) {
      console.log(error);
    }
  };

  const handleAddData = async () => {
    const newData = { name, email, password };
    try {
      await axios.post('http://dndoverlord.ru:3022/api/users', newData);
      fetchData();
      form.resetFields();
    } catch (error) {
      console.log(error);
      form.resetFields();
    }
  };

  const handleDelete = async (record) => {
    try {
      // Отправляем запрос на удаление строки на сервер
      await axios.delete(`http://dndoverlord.ru:3022/api/users/${record.id}`);
      fetchData();
    } catch (error) {
      console.error(error);
    }
  };

  const handleOk = () => {
    setConfirmLoading(true);
    setTimeout(() => {
      handleAddData();
      setModalVisible(false);
      setConfirmLoading(false);
    }, 1000);
  };

  const handleModalCancel = () => {
    setModalVisible(false)
    form.resetFields()
  };

  return (
    <>
      <HeaderComponent />
      <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
        <Button onClick={() => setModalVisible(true)}>Создать аккаунт</Button>

        <Modal
          title="Создать аккаунт"
          open={modalVisible}
          onCancel={() => handleModalCancel()}
          onOk={handleOk}
          confirmLoading={confirmLoading}
        >
          <Form onFinish={handleAddData} form={form}>
            <Form.Item name="name" label="Name" rules={[{ required: true }]}>
              <Input
                placeholder="Name"
                maxLength="36"
                value={name}
                onChange={(e) => setName(e.target.value)}
              />
            </Form.Item>

            <Form.Item name="email" label="Email" rules={[{ required: true, type: 'email' }]}>
              <Input
                placeholder="Email"
                maxLength="48"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />
            </Form.Item>

            <Form.Item name="password" label="Password" rules={[{ required: true }]}>
              <Input.Password
                placeholder="Password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />
            </Form.Item>
          </Form>
        </Modal>

        <Table
          rowKey="id"
          columns={columns}
          dataSource={data}
          pagination={{
            showSizeChanger: true,
          }}
          loading={loading}
        />
      </Space>
    </>
  );
};

export default AccTableComponent;