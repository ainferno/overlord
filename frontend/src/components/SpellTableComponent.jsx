import React, { useState, useEffect } from 'react';
import { Button, Modal, Form, Table, Space, Checkbox, Input } from 'antd';
import { SearchOutlined } from '@ant-design/icons';
import axios from 'axios';
import ReactHTMLParser from 'react-html-parser';

import { CustomTable, Search} from './styles/SpellTableStyles';

const SpellTableComponent = () => {
    const [data, setData] = useState([]);
    const [filteredData, setFilteredData] = useState([]);
    const [modalVisible, setModalVisible] = useState(false);
    const [spellTitle, setSpellTitle] = useState('');
    const [spellDescription, setSpellDescription] = useState('');

    // данные для отображения в таблице
    const dataSource = data.flatMap(item => item.spells.map(spell => ({
        level: spell.level,
        name_rus: spell.name_rus,
        prepared: spell.prepared,
        id: spell.id,
    })));

    const url_characters = process.env.REACT_APP_ADDR + "/api/characters";
    const url_spell = process.env.REACT_APP_ADDR + "/api/spells";

    const columns = [
        {
            title: 'Уровень',
            dataIndex: 'level',
            key: 'level',
            ellipsis: true,
            width: '25%',
            sorter: {
                compare: (a, b) => a.level - b.level,
                multiple: 1,
            },
            filters: [
                { text: '1', value: 1 },
                { text: '2', value: 2 },
                { text: '3', value: 3 },
                { text: '4', value: 4 },
                { text: '5', value: 5 },
                { text: '6', value: 6 },
            ],
            onFilter: (value, record) => record.level === value,
        },
        {
            title: 'Заклинание',
            dataIndex: 'name_rus',
            key: 'name_rus',
            ellipsis: true,
            render: (name, record) => (
                <Button onClick={() => handleSpellClick(record)}>{name}</Button>
            ),
            filterDropdown: ({ setSelectedKeys, selectedKeys, confirm, clearFilters }) => (
                <div style={{ padding: 8 }}>
                    <Search
                        placeholder="Search name"
                        value={selectedKeys[0]}
                        onChange={(e) => setSelectedKeys(e.target.value ? [e.target.value] : [])}
                        onPressEnter={() => confirm()}
                        style={{ width: 188, marginBottom: 8, display: 'block' }}
                    />
                    <Button
                        type="primary"
                        onClick={() => confirm()}
                        size="small"
                        style={{ width: 90, marginRight: 8 }}
                    >
                        Search
                    </Button>
                    <Button onClick={() => clearFilters()} size="small" style={{ width: 90 }}>
                        Reset
                    </Button>
                </div>
            ),
            filterIcon: filtered => (
                <SearchOutlined style={{ color: filtered ? '#1890ff' : undefined }} />
            ),
            onFilter: (value, record) =>
                record.name_rus.toLowerCase().includes(value.toLowerCase()),
        },
        {
            title: 'Подготовка',
            dataIndex: 'prepared',
            key: 'prepared',
            ellipsis: true,
            width: '25%',
            sorter: {
                compare: (a, b) => a.prepared - b.prepared,
                multiple: 2,
            },
            filters: [
                { text: 'Prepared', value: true },
                { text: 'Not Prepared', value: false },
            ],
            onFilter: (value, record) => record.prepared === value,
            render: (prepared, record) => (
                <Checkbox
                    checked={prepared}
                    onChange={(e) => handleCheckboxChange(record, e.target.checked)}
                />
            ),
        },
    ];

    // функцию `handleSpellClick`, открывает модальное окно и устанавливать выбранное заклинание:
    const handleSpellClick = async (record) => {
        try {
            const response = await axios.get(url_spell + `/${record.id}`);
            setSpellDescription(response.data.description);
            setSpellTitle(response.data.name_rus);
            setModalVisible(true);
            console.log(spellDescription)
        } catch (error) {
            console.log(error);
        }
    };

    const handleCheckboxChange = (record, checked) => {
        const updatedData = data.map(item => {
            const updatedSpells = item.spells.map(spell => {
                if (spell.id === record.id) {
                    return {
                        ...spell,
                        prepared: checked,
                    };
                }
                return spell;
            });
            return {
                ...item,
                spells: updatedSpells,
            };
        });
        setData(updatedData);
        setFilteredData(updatedData);
    };

    const fetchData = async () => {
        try {
            const response = await axios.get(url_characters);
            setData(response.data);
        } catch (error) {
            console.log(error);
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <>
            <Space direction="vertical" size="middle" style={{ display: 'flex' }}>
                <CustomTable
                    rowKey="id"
                    columns={columns}
                    dataSource={dataSource}
                    pagination={{
                        showSizeChanger: true,
                    }}
                />
            </Space>
            <Modal
                title="Описание заклинания"
                open={modalVisible}
                onCancel={() => setModalVisible(false)}
                footer={null}
            >
                <h2>{spellTitle}</h2>
                {ReactHTMLParser(spellDescription)}
            </Modal>
        </>
    );
};

export default SpellTableComponent;