import React from 'react'
import { BrowserRouter, Route, Routes, Link } from 'react-router-dom';

// import Homepage from '../pages/Homepage_old';
import AccTableComponent from '../components/AccTableComponent';
import LoginPage from '../pages/Loginpage';
import Spellpage from '../pages/Spellpage';
// import Characterspage from '../pages/Characterspage';
import HomePage from '../pages/homePage';
import LobbyPage from '../pages/lobbyPage';
import CharactersPage from '../pages/charactersPage';
import AboutUs from '../pages/aboutUs';

function MainRouter() {
    return (
        <>
            <Routes>
                <Route path="/" element={<HomePage />} />
                <Route path="/lobby" element={<LobbyPage />} />
                <Route path="/acctable" element={<AccTableComponent />} />
                <Route path="/spell" element={<Spellpage />} />
                <Route path="/characters" element={<CharactersPage/>} />

                <Route path="/aboutus" element={<AboutUs />} />
                <Route path="/login" element={<LoginPage />} />
            </Routes>
        </>
    )
}

export default MainRouter