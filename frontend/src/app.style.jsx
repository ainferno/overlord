import styled, { createGlobalStyle } from 'styled-components';

export const GlobalStyles = createGlobalStyle`
body {
    margin: 0; line-height: normal;
    background-color: var(--grey-10); /* сделал глобальный цвет*/
    }
:root {
/* fonts */

--font-lexend: Lexend;
--font-inter: Inter;

/* font sizes */

--font-size-lg: 18px;
--font-size-xl: 20px;
--font-size-29xl: 48px;
--font-size-3xl: 22px;
--font-size-39xl: 58px;
--font-size-11xl: 30px;
--font-size-5xl: 24px;
--font-size-base-6: 15.6px;
--font-size-sm-9: 13.9px;
--font-size-mid-3: 17.3px;
--font-size-xl-8: 20.8px;

/* Colors */

--grey-15: #262626;
--color-gray-100: #22251b;
--grey-11: #1c1c1c;
--grey-10: #1a1a1a;
--absolute-white: #fff;
--green-65: #d1ff4d;
--green-60: #caff33;
--grey-70: #b3b3b3;
--white-shades-90: #e4e4e7;
--grey-90: #e6e6e6;
--absolute-white: #fff;

/* Gaps */

--gap-7xl: 26px;
--gap-11xl: 30px;
--gap-61xl: 80px;
--gap-sm: 14px;
--gap-3xs: 10px;
--gap-5xl: 24px;
--gap-81xl: 100px;
--gap-xl: 20px;
--gap-43xl: 62px;
--gap-31xl: 50px;
--gap-8xs: 5px;
--gap-7xs: 6px;
--gap-281xl: 300px;
--gap-4xs-7: 8.7px;

/* Paddings */

--padding-xl: 20px;
--padding-xs: 12px;
--padding-5xl: 24px;
--padding-sm: 14px;
--padding-11xl: 30px;
--padding-lg: 18px;
--padding-31xl: 50px;
--padding-281xl: 300px;
--padding-3xs: 10px;
--padding-81xl: 100px;
--padding-143xl: 162px;
--padding-base: 16px;
--padding-15xl-7: 34.7px;
--padding-4xs-7: 8.7px;
--padding-xl-8: 20.8px;
--padding-sm-9: 13.9px;
--padding-mid-3: 17.3px;
--padding-7xl: 26px;
    
/* Border radiuses */

--br-81xl: 100px;
--br-63xl: 82px;
--br-sm: 14px;
--br-xs: 12px;
--br-xl: 20px;
--br-base: 16px;
--br-51xl: 70px;
--br-31xl: 50px;
--br-7xs: 6px;
--br-121xl: 140px;
--br-2xs-4: 10.4px;
--br-27xl: 46px;
}
`;