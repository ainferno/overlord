import React from 'react'
import Navbar from '../components/navbar/navbar';
import Footer from '../components/footer/footer';

function CharactersPage() {
    return (
        <>
            <Navbar />

            <Footer />
        </>
    )
}

export default CharactersPage;