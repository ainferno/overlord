import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Row, Col, Button } from 'antd';

import { GlobalStyle, StyledCarousel, Slide, Title, StatValue } from '../styles/CharacterpageStyled';

import HeaderComponent from '../components/HeaderComponent';
import SpellTableComponent from '../components/SpellTableComponent';

function Characterspage() {
  const [characters, setCharacters] = useState([]);

  const url_characters = process.env.REACT_APP_ADDR + "/api/characters";

  // передача данных
  useEffect(() => {
    fetchData();
  }, []);

  const fetchData = () => {
    axios.get(url_characters)
      .then(response => {
        setCharacters(response.data);
      })
      .catch(error => {
        console.log(error);
      });
  };

  // ставит -/+ для статуса
  const printStat = (stat) => {
    return stat > 0 ? "+" + stat : stat
  }

  // кнопки переключения здоровья
  const handleIncreaseHealth = (characterId, amount) => {
    axios.put(`${url_characters}`, { health: characters.find(character => character.id === characterId).health + amount })
      .then(response => {
        fetchData();
      })
      .catch(error => {
        console.log(error);
      });
  };

  const handleDecreaseHealth = (characterId, amount) => {
    axios.put(`${url_characters}/${characterId}`, { health: characters.find(character => character.id === characterId).health - amount })
      .then(response => {
        fetchData();
      })
      .catch(error => {
        console.log(error);
      });
  };;

  // настройки слайдера
  const responsiveSettings = {
    dots: false,
    infinite: true,
    speed: 600,
    slidesToShow: 1,
    slidesToScroll: 1,
    adaptiveHeight: true,
  };

  return (
    <>
      <GlobalStyle />
      <HeaderComponent />
      {characters.map(character => (
        <div key={character.id}>
          <h1>{character.name}</h1>
          <div style={{ display: 'flex', justifyContent: 'space-between', alignItems: 'center' }}>
            <div>
              <Button type="primary" onClick={() => handleIncreaseHealth(character.id, 10)}>+10</Button>
              <Button type="primary" onClick={() => handleIncreaseHealth(character.id, 5)}>+5</Button>
              <Button type="primary" onClick={() => handleIncreaseHealth(character.id, 1)}>+1</Button>
            </div>
            <h1>Здоровье: {character.health}</h1>
            <div>
              <Button type="primary" onClick={() => handleDecreaseHealth(character.id, 1)}>-1</Button>
              <Button type="primary" onClick={() => handleDecreaseHealth(character.id, 5)}>-5</Button>
              <Button type="primary" onClick={() => handleDecreaseHealth(character.id, 10)}>-10</Button>
            </div>
          </div>
        </div>
      ))}
      {characters.map(character => (
        <Row key={character.id}>
          <Col span={24}>
            {character.classes.map(character => (
              <Row key={character.id}>
                <Col span={24}>
                  <h1>{character.class.name_rus}-{character.name_rus}-{character.level}</h1>
                </Col>
              </Row>
            ))}
          </Col>
        </Row>
      ))}

      <StyledCarousel draggable={true} {...responsiveSettings}>
        {characters.map(character => (
          <Slide key={character.id}>
            <Row>
              <Col span={24}>
                <h1>{character.name}</h1>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <Title>Сила: {printStat(character.stats.str_mod)}</Title>
                <StatValue>Атлетика: {character.skills.athletics}</StatValue>
              </Col>
              <Col span={12}>
                <Title>Харизма: {printStat(character.stats.cha_mod)}</Title>
                <StatValue>Обман: {printStat(character.skills.deception)}</StatValue>
                <StatValue>Запугивание: {printStat(character.skills.intimidation)}</StatValue>
                <StatValue>Выступление: {printStat(character.skills.perfomance)}</StatValue>
                <StatValue>Убеждение: {printStat(character.skills.persuation)}</StatValue>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <Title>Телосложение: {printStat(character.stats.con_mod)}</Title>
              </Col>
              <Col span={12}>
                <Title>Ловкость: {printStat(character.stats.dex_mod)}</Title>
                <StatValue>Акробатика: {printStat(character.skills.acrobatics)}</StatValue>
                <StatValue>Ловкость рук: {printStat(character.skills.sleight_of_hand)}</StatValue>
                <StatValue>Скрытность: {printStat(character.skills.stealth)}</StatValue>
              </Col>
            </Row>
            <Row>
              <Col span={12}>
                <Title>Мудрость: {printStat(character.stats.wiz_mod)}</Title>
                <StatValue>Обращение с животными: {printStat(character.skills.animal_handling)}</StatValue>
                <StatValue>Проницательность: {printStat(character.skills.insight)}</StatValue>
                <StatValue>Медицина: {printStat(character.skills.medicine)}</StatValue>
                <StatValue>Внимание: {printStat(character.skills.perception)}</StatValue>
                <StatValue>Выживание: {printStat(character.skills.survival)}</StatValue>
              </Col>
              <Col span={12}>
                <Title>Интеллект: {printStat(character.stats.int_mod)}</Title>
                <StatValue>Магия: {printStat(character.skills.arcana)}</StatValue>
                <StatValue>История: {printStat(character.skills.history)}</StatValue>
                <StatValue>Анализ: {printStat(character.skills.investigation)}</StatValue>
                <StatValue>Природа: {printStat(character.skills.nature)}</StatValue>
                <StatValue>Религия: {printStat(character.skills.religion)}</StatValue>
              </Col>
            </Row>
          </Slide>
        ))}
        <Slide>
          <SpellTableComponent />
        </Slide>
      </StyledCarousel >
    </>
  )
}

export default Characterspage;