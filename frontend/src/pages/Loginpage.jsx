import React, { useState } from 'react';
import { Form, Input, Button, Checkbox } from 'antd';
import axios from 'axios';
import { useNavigate  } from 'react-router-dom';

import { GlobalStyle, Container, LoginForm, ForgotPassword, Register } from './styles/LoginpageStyle'

const LoginPage = () => {
  const [isValidCredentials, setIsValidCredentials] = useState(null);
  const navigate  = useNavigate();

  const url_characters = process.env.REACT_APP_ADDR + "/api/users";

  const onFinish = async (values) => {
    try {
      const response = await axios.post(
        url_characters,
        values
      );

      if (response.status === 200) {
        setIsValidCredentials(true);
        navigate('/');
      } else {
        setIsValidCredentials(false);
      }
    } catch (error) {
      console.error('Ошибка при проверке учетных данных:', error);
      setIsValidCredentials(false);
    }
  };

  const handleReset = () => {
    setIsValidCredentials(null);
  };

  return (
    <>
      {/* <GlobalStyle /> */}
      <Container>
        <LoginForm>
          <Form
            name="login"
            initialValues={{ remember: true }}
            onFinish={onFinish}
            onReset={handleReset}
          >
            <Form.Item
              name="name"
              rules={[{ required: true, message: 'Please enter your name' }]}
            >
              <Input placeholder="Name" />
            </Form.Item>

            <Form.Item
              name="email"
              rules={[{ required: true, message: 'Please enter your email' }]}
            >
              <Input placeholder="Email" />
            </Form.Item>

            <Form.Item
              name="password"
              rules={[{ required: true, message: 'Please enter your password' }]}
            >
              <Input.Password placeholder="Password" />
            </Form.Item>

            {/* <Form.Item>
              <Checkbox>Remember me</Checkbox>
            </Form.Item> */}

            <Form.Item>
              <Button type="primary" htmlType="submit">
                Log in
              </Button>
            </Form.Item>
          </Form>

          <ForgotPassword>Forgot username or password?</ForgotPassword>
          <Register>Register</Register>
        </LoginForm>
      </Container>
    </>
  );
};

export default LoginPage;