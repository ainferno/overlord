import React from 'react';

import HeaderComponent from '../components/HeaderComponent';
import SpellTableComponent from '../components/SpellTableComponent';

const Spellpage = () => {
    return (
        <>
            <HeaderComponent />
            <SpellTableComponent />
        </>
    );
};

export default Spellpage;