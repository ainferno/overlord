import React from 'react';
import { Link } from 'react-router-dom';
import { Layout, Menu } from 'antd';
import { HomeOutlined, InfoCircleOutlined, LoginOutlined, TableOutlined } from '@ant-design/icons';
import styled, { createGlobalStyle } from 'styled-components';

import HeaderComponent from '../components/HeaderComponent';

const { Header, Content, Footer } = Layout;

const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
  }
`;

const PageContainer = styled.div`
  display: flex;
  flex-direction: column;
  min-height: 100vh;
`;

const Logo = styled.div`
  width: 120px;
  height: 31px;
  background: #fff;
  margin: 16px 100px 16px 0;
  float: left;
`;

const StyledHeader = styled(Header)`
  width: 100%;
  height: 64px;
  // display: flex;
  // align-items: center;

  // @media (min-width: 768px) {
  //   height: 60px;
  // }
`;

const StyledFooter = styled(Footer)`
  text-align: center;
`;

// const Header = styled.header`
//   width: 100%;
//   height: 69px;
//   background-color: #f2f2f2;
// `;

const Main = styled.main`
  flex: 1;
  background-color: #ffffff;
`;

// const Footer = styled.footer`
//   width: 100%;
//   height: 69px;
//   background-color: #f2f2f2;
// `;

const CustomMenu = styled(Menu)`
  height: 60px;
`;

const HomePage = () => {
  return (
    <>
      <GlobalStyle />
      <Layout>
        <PageContainer>
          <HeaderComponent/>
          {/* <HeaderComponent_test /> */}
        </PageContainer>
      </Layout>
    </>

  );
};

export default HomePage;