import React from 'react'
import Navbar from '../components/navbar/navbar';
import Footer from '../components/footer/footer';

import {
    AbstractDesignIcon, Heading, Text1, Button, SubContainer1,
    Container1, Heading1, HeroSectionRoot, PageContainer
} from './styles/homePageStyle';
import AbstractDesign from "../assets/AbstractDesign.svg"
import OurServicesSection from './homePage/parts/OurServices';

function HomePage() {
    return (
        <>
            <Navbar />
            <PageContainer>
                <HeroSectionRoot>
                    <Container1>
                        <SubContainer1>
                            <Heading>Храни свои приключения в одном месте</Heading>
                            <Heading1>Создай своих легендарных персонажей и совместные лобби для игры в DnD - воплоти свои фантазии и стань частью увлекательного мира приключений!</Heading1>
                            <Button>
                                <Text1>Создать персонажа</Text1>
                            </Button>
                        </SubContainer1>
                    </Container1>
                    <AbstractDesignIcon alt="" src={AbstractDesign} />
                </HeroSectionRoot>
            </PageContainer>
            {/*  */}
            <OurServicesSection />
            <Footer />
        </>
    )
}

export default HomePage;