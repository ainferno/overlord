import styled, {createGlobalStyle} from 'styled-components';

export const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
  }
`;

export const Container = styled.div`
  background-color: #f5f5f5;
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
`;

export const LoginForm = styled.div`
  background-color: #ffffff;
  padding: 40px;
  border-radius: 4px;
`;

export const ForgotPassword = styled.p`
  text-align: center;
  margin-bottom: 8px;
`;

export const Register = styled.p`
  text-align: center;
`;