import React from 'react'
import Navbar from '../components/navbar/navbar';
import Footer from '../components/footer/footer';

function LobbyPage() {
    return (
        <>
            <Navbar />

            <Footer />
        </>
    )
}

export default LobbyPage;