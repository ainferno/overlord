import styled from "styled-components";
import OurServiceImage from '../../../assets/OurServiceImage.svg';
import LobbyIcon from '../../../assets/LobbyIcon.svg';
import CharacterIcon from '../../../assets/CharacterIcon.svg';
import DevicePhoneMobile from '../../../assets/DevicePhoneMobile.svg';

const OurServicesSection = () => {
    return (
        <OurServicesSection1Root>
            <OurServicesSectionContainer1>
                <TextContainer5>
                    <VectorIcon alt="" src={OurServiceImage} />
                    <Paragraph>{`Создай свои лобби с большими мирами, приключениями и уникальными персонажами `}</Paragraph>
                    <Heading>Наши услуги</Heading>
                </TextContainer5>
                <TextContainer6>
                    <Heading1>Наши услуги</Heading1>
                    <Paragraph1>{`Создай свои лобби с большими мирами, приключениями и уникальными персонажами `}</Paragraph1>
                </TextContainer6>
                <Container4>
                    <Card>
                        <Container5>
                            <Icon3>
                                <Icon4 alt="" src={LobbyIcon} />
                            </Icon3>
                            <TextContainer7>
                                <Heading2>Лобби</Heading2>
                                <Paragraph1>
                                    <P>Мы предлагаем уникальный функционал, который поможет тебе организовать эпические приключения.</P>
                                    <P>&nbsp;</P>
                                    <P>Создай свое собственное лобби и отправляйся в увлекательное путешествие вместе с нами. На нашем сайте ты найдешь все необходимое для организации и настройки лобби, чтобы каждая игра в DnD стала настоящим приключением. Присоединяйся к нам сегодня и воплоти свои фантазии в реальность!</P>
                                </Paragraph1>
                            </TextContainer7>
                        </Container5>
                    </Card>
                    <Line2 />
                    <Card>
                        <Container5>
                            <Icon3>
                                <Icon4 alt="" src={CharacterIcon} />
                            </Icon3>
                            <TextContainer7>
                                <Heading2>Персонажи</Heading2>
                                <Paragraph1>
                                    <P>Наша команда готова воплотить все твои фантазии и помочь тебе создать героя, который будет отражать твою уникальность и стиль игры.</P>
                                    <P>&nbsp;</P>
                                    <P>{`Наши услуги создания персонажей помогут тебе воплотить все твои идеи и стать частью увлекательного мира приключений в Dungeons & Dragons. Воплоти свои мечты, стань героем своей собственной истории - присоединяйся к нам прямо сейчас!`}</P>
                                </Paragraph1>
                            </TextContainer7>
                        </Container5>
                    </Card>
                    <Card2>
                        <Container5>
                            <Icon3>
                                <Icon4 alt="" src={DevicePhoneMobile} />
                            </Icon3>
                            <TextContainer7>
                                <Heading2>Мобильная версия</Heading2>
                                <Paragraph1>
                                    <P>Наш сайт предлагает удобство и доступность для всех игроков, независимо от того, на каком устройстве они играют. Мы гордимся тем, что наш сайт полностью адаптирован под все девайсы, включая компьютеры, планшеты и мобильные устройства.</P>
                                    <P>&nbsp;</P>
                                    <P>{`Присоединяйся к нашему сообществу и наслаждайся удобством использования на любом устройстве. `}</P>
                                </Paragraph1>
                            </TextContainer7>
                        </Container5>
                    </Card2>
                    <Line3 />
                </Container4>
            </OurServicesSectionContainer1>
        </OurServicesSection1Root>);
};

export default OurServicesSection;

const VectorIcon = styled.img`position: absolute;
height: 100%;
width: 100%;
top: 0%;
right: 0%;
bottom: 0%;
left: 0%;
max-width: 100%;
overflow: hidden;
max-height: 100%;
object-fit: cover;
mix-blend-mode: overlay;
`;
const Paragraph = styled.div`position: absolute;
height: 15.11%;
width: 100%;
top: 58.94%;
left: 0%;
letter-spacing: -0.01em;
line-height: 24px;
display: inline-block;
`;
const Heading = styled.div`position: absolute;
height: 18.26%;
width: 100%;
top: 40.81%;
left: -0.08%;
font-size: var(--font-size-29xl);
font-weight: 600;
color: var(--absolute-white);
display: inline-block;
`;
const TextContainer5 = styled.div`position: relative;
width: 1200px;
height: 397px;
overflow: hidden;
flex-shrink: 0;
color: var(--grey-90);
`;
const Heading1 = styled.div`align-self: stretch;
position: relative;
font-weight: 600;
`;
const Paragraph1 = styled.div`align-self: stretch;
position: relative;
font-size: var(--font-size-lg);
letter-spacing: -0.01em;
line-height: 24px;
color: var(--grey-90);
`;
const TextContainer6 = styled.div`align-self: stretch;
display: none;
flex-direction: column;
align-items: center;
justify-content: flex-start;
padding: var(--padding-101xl) 300px;
gap: 14px;
background-image: url('Text Container.png');
background-size: cover;
background-repeat: no-repeat;
background-position: top;
font-size: var(--font-size-29xl);
`;
const Icon4 = styled.img`position: relative;
width: 40px;
height: 40px;
overflow: hidden;
flex-shrink: 0;
`;
const Icon3 = styled.div`
position: relative;
border-radius: 10px;
background: linear-gradient(170deg, #242424, rgba(36, 36, 36, 0)), linear-gradient(220deg, rgba(158, 255, 0, 0.2) 10%, rgba(158, 255, 0, 0) 30%);
// width: 100%;
overflow: hidden;
display: flex;
flex-direction: column;
align-items: flex-start;
justify-content: flex-start;
padding: 24px;
margin-bottom: 40px;
box-sizing: border-box;
`;
const Heading2 = styled.div`align-self: stretch;
position: relative;
letter-spacing: -0.01em;
line-height: 150%;
font-weight: 600;
`;
const P = styled.p`margin: 0;
`;
const TextContainer7 = styled.div`align-self: stretch;
display: flex;
flex-direction: column;
align-items: flex-start;
justify-content: flex-start;
gap: var(--gap-xl);
`;
const Container5 = styled.div`align-self: stretch;
display: flex;
flex-direction: column;
align-items: flex-start;
justify-content: flex-start;
gap: var(--gap-21xl);
`;
const Card = styled.div`align-self: stretch;
flex: 1;
display: flex;
flex-direction: column;
align-items: flex-start;
justify-content: flex-start;
padding: var(--padding-31xl);
`;
const Line2 = styled.div`align-self: stretch;
position: relative;
border-right: 1px solid var(--grey-15);
box-sizing: border-box;
width: 0px;
`;
const Icon5 = styled.div`border-radius: var(--br-3xs);
background: linear-gradient(229.29deg, rgba(158, 255, 0, 0.2) 22.41%, rgba(158, 255, 0, 0) 68.71%), linear-gradient(180deg, #242424, rgba(36, 36, 36, 0));
display: flex;
flex-direction: column;
align-items: flex-start;
justify-content: flex-start;
padding: var(--padding-5xl);
`;
const Card2 = styled.div`align-self: stretch;
flex: 1;
border-left: 1px solid var(--grey-15);
display: flex;
flex-direction: column;
align-items: flex-start;
justify-content: space-between;
padding: var(--padding-31xl);
`;
const Line3 = styled.div`align-self: stretch;
position: relative;
border-right: 1px solid var(--grey-15);
box-sizing: border-box;
width: 1px;
`;
const Container4 = styled.div`align-self: stretch;
border-top: 1px solid var(--grey-15);
display: flex;
flex-direction: row;
align-items: flex-start;
justify-content: flex-start;
text-align: left;
font-size: var(--font-size-11xl);
`;
const OurServicesSectionContainer1 = styled.div`border: 1px solid var(--grey-15);
box-sizing: border-box;
width: 1202px;
height: 1122px;
display: flex;
flex-direction: column;
align-items: center;
justify-content: flex-start;
`;
const OurServicesSection1Root = styled.div`position: relative;
width: 100%;
height: 1050px;
overflow: hidden;
display: flex;
flex-direction: column;
align-items: center;
justify-content: flex-start;
padding: 0px var(--padding-101xl);
box-sizing: border-box;
text-align: center;
font-size: var(--font-size-lg);
color: var(--absolute-white);
font-family: var(--font-barlow);
margin: 100px 0;
`;
