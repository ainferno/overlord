import styled, { createGlobalStyle } from 'styled-components';
import { Carousel } from 'antd';

export const GlobalStyle = createGlobalStyle`
  body {
    margin: 0;
    padding: 0;
  }
`;

export const StyledCarousel = styled(Carousel)`
  @media (hover: hover) {
    .slick-slide > div {
      pointer-events: none;
    }
  }
`;

export const Slide = styled.div`
  height: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #f5f5f5;
  text-align: center;
`;

export const Title = styled.h2`
  font-size: 20px;
  font-weight: bold;
  margin-bottom: 10px;
`;

export const StatValue = styled.p`
  font-size: 16px;
  margin-bottom: 10px;
`;
