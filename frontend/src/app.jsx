import React from 'react'
import { BrowserRouter } from 'react-router-dom';
import { GlobalStyles } from './app.style';

import MainRouter from './routers/MainRouter';

function App() {
  return (
    <>
      <GlobalStyles />
      <BrowserRouter>
        <MainRouter />
      </BrowserRouter>
    </>
  )
}

export default App;