package main

import (
	"encoding/json"
	"fmt"
	"math"
	"strconv"
	"strings"
)

func StatToMod(stat int) int {
	return int(math.Floor(float64(stat-10) / 2.0))
}

func main() {
	// for i := 1; i <= 30; i++ {
	// 	fmt.Printf("STAT = %d, MOD = %d\n", i, StatToMod(i))
	// }
	reader := strings.NewReader("{\"id\":\"5\",\"name\":\"test\"}")
	var res interface{}
	err := json.NewDecoder(reader).Decode(&res)
	if err != nil {
		fmt.Println("ERR: ", err.Error())
	} else {
		fmt.Println("RES:", res)
		params := res.(map[string]interface{})

		id, _ := strconv.Atoi(params["id"].(string))

		fmt.Println("params.id:", id)
		fmt.Println("params.name:", params["name"].(string))
	}
}
