package main

import (
	"bufio"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"regexp"
	"strconv"
	"strings"

	"gorm.io/driver/postgres"
	"gorm.io/gorm"
)

const DURATION_TYPE_OTHER = 13
const TIMECAST_TYPE_OTHER = 11
const DISTANCE_TYPE_OTHER = 18

type SpellData struct {
	Name struct {
		Rus string `json:"rus"`
		Eng string `json:"eng"`
	} `json:"name"`
	Level      int    `json:"level"`
	School     string `json:"school"`
	Components struct {
		V bool `json:"v"`
		S bool `json:"s"`
		M bool `json:"m"`
	} `json:"components"`
	Ritual        bool   `json:"ritual"`
	Concentration bool   `json:"concentration"`
	URL           string `json:"url"`
	Source        struct {
		ShortName string `json:"shortName"`
		Name      string `json:"name"`
	} `json:"source"`
}

type UiqueSpellData struct {
	Name struct {
		Rus string `json:"rus"`
		Eng string `json:"eng"`
	} `json:"name"`
	Level          int    `json:"level"`
	School         string `json:"school"`
	AdditionalType string `json:"additionalType"`
	Components     struct {
		V bool   `json:"v"`
		S bool   `json:"s"`
		M string `json:"m"`
	} `json:"components"`
	URL    string `json:"url"`
	Source struct {
		ShortName string `json:"shortName"`
		Name      string `json:"name"`
		Homebrew  bool   `json:"homebrew"`
	} `json:"source"`
	Range    string `json:"range"`
	Duration string `json:"duration"`
	Time     string `json:"time"`
	Classes  []struct {
		Name string `json:"name"`
		URL  string `json:"url"`
		Icon string `json:"icon"`
	} `json:"classes"`
	Subclasses []struct {
		Name  string `json:"name"`
		URL   string `json:"url"`
		Class string `json:"class"`
	} `json:"subclasses"`
	Description   string `json:"description"`
	Upper         string `json:"upper"`
	Ritual        bool   `json:"ritual"`
	Сoncentration bool   `json:"concentration"`
}

type ClassData struct {
	Name struct {
		Rus string `json:"rus"`
		Eng string `json:"eng"`
	} `json:"name"`
	URL    string `json:"url"`
	Source struct {
		ShortName string `json:"shortName"`
		Name      string `json:"name"`
	} `json:"source"`
	Dice       string `json:"dice"`
	Archetypes []struct {
		Name struct {
			Rus string `json:"rus"`
			Eng string `json:"eng"`
		} `json:"name"`
		Type struct {
			Name  string `json:"name"`
			Order int    `json:"order"`
		} `json:"type"`
		Source struct {
			ShortName string `json:"shortName"`
			Name      string `json:"name"`
		} `json:"source"`
		URL string `json:"url"`
	} `json:"archetypes"`
	Image         string `json:"image"`
	Icon          string `json:"icon"`
	ArchetypeName string `json:"archetypeName"`
	Sidekick      bool   `json:"sidekick"`
}

type School struct {
	ID   int
	Name string
	Rus  string
}

type DamageType struct {
	ID   int
	Name string
	Rus  string
}

type TimecastType struct {
	ID   int
	Name string
	Rus  string
}

type DistanceType struct {
	ID   int
	Name string
}

type DurationType struct {
	ID   int
	Name string
}

type Material struct {
	ID         int
	Text       string
	Cost       int
	Expendable bool
}

type Spell struct {
	ID            int
	Name          string
	Rus           string
	Level         int
	School        int
	Ritual        bool
	Concentration bool
	Damage        int
	Timecast      int
	TimecastFull  string
	Distance      int
	DistanceFull  string
	Duration      int
	DurationFull  string
	Verbal        bool
	Somatic       bool
	Material      int
	Description   string
	Upper         string
	Url           string
}

type ClassSpell struct {
	Class int
	Spell int
}

type Class struct {
	ID   int
	Name string
	Rus  string
	Dice int
}

type ClassArchetype struct {
	ID    int
	Class int
	Name  string
	Rus   string
}

type Abilities struct {
	Key       string `json:"key"`
	Name      string `json:"name"`
	ShortName string `json:"shortName"`
	Value     int    `json:"value"`
}

type RaceData struct {
	Name struct {
		Rus string `json:"rus"`
		Eng string `json:"eng"`
	} `json:"name"`
	URL       string      `json:"url"`
	Abilities []Abilities `json:"abilities"`
	Type      struct {
		Name  string `json:"name"`
		Order int    `json:"order"`
	} `json:"type"`
	Source struct {
		ShortName string `json:"shortName"`
		Name      string `json:"name"`
	} `json:"source"`
	Subraces []struct {
		Name struct {
			Rus string `json:"rus"`
			Eng string `json:"eng"`
		} `json:"name"`
		URL       string      `json:"url"`
		Abilities []Abilities `json:"abilities"`
		Type      struct {
			Name  string `json:"name"`
			Order int    `json:"order"`
		} `json:"type"`
		Source struct {
			ShortName string `json:"shortName"`
			Name      string `json:"name"`
		} `json:"source"`
		Image string `json:"image"`
	} `json:"subraces"`
	Image string `json:"image"`
}

type Race struct {
	ID     int
	Name   string
	Rus    string
	URL    string
	Type   string
	Source string
}

type RacesAbilities struct {
	ID        int
	Race      int
	Key       string
	Name      string
	ShortName string
	Value     int
}

func connectDB() *gorm.DB {
	// dbHost := "localhost"
	// dbPort := "5432"
	// dbName := "overlord"
	// dbUsername := "ainferno"
	// dbPassword := "Katran1998"

	// dsn := fmt.Sprintf("host=%s user=$s password=$s dbname=$s port=$s sslmode=disable TimeZone=Asia/Shanghai", dbUsername, dbPassword, dbHost, dbPort, dbName)
	dsn := "host=localhost user=overlord password=overlord dbname=overlord port=5432 sslmode=disable TimeZone=Europe/Moscow"

	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("ERROR: db connection error")
	}

	return db
}

func getData(URL string, data []byte) []byte {
	client := &http.Client{}

	req, err := http.NewRequest("POST", URL, bytes.NewBuffer(data))
	if err != nil {
		fmt.Println("ERR: ", err)
	}

	req.Header.Add("content-type", "application/json")

	resp, err := client.Do(req)
	if err != nil {
		fmt.Println("ERR: ", err)
	}
	defer resp.Body.Close()

	b, err := io.ReadAll(resp.Body)
	if err != nil {
		fmt.Println("ERR: ", err)
	}

	return b
}

func fillClases(db *gorm.DB) {
	data := getData("https://ttg.club/api/v1/classes", []byte(`{"page":0,"limit":-1,"search":{"value":"","exact":false}}`))

	var classes []ClassData
	err := json.Unmarshal(data, &classes)
	if err != nil {
		fmt.Println("ERR: ", err)
	}

	class := Class{Name: "OTHER", Rus: "особый"}
	result := db.Table("classes").Create(&class)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)

	for _, v := range classes {
		if !v.Sidekick {
			dice, err := strconv.Atoi(v.Dice[2:])
			if err != nil {
				fmt.Println("Error: ", err)
			}
			class := Class{Name: v.Name.Eng, Rus: v.Name.Rus, Dice: dice}

			result := db.Table("classes").Create(&class)
			if result.Error != nil {
				fmt.Println("Error creating", v.Name.Eng, " - ", result.Error)
			} else {
				fmt.Printf("Succesfully created class %s, id = %d\n", class.Name, class.ID)
			}
			for _, a := range v.Archetypes {
				archetype := ClassArchetype{Name: a.Name.Eng, Rus: a.Name.Rus, Class: class.ID}
				result = db.Table("class_archetypes").Create(&archetype)
				if result.Error != nil {
					fmt.Println("Error creating", a.Name.Eng, " - ", result.Error)
				} else {
					fmt.Printf("Succesfully created archetype %s, id = %d, for class %s\n", archetype.Name, archetype.ID, class.Name)
				}
			}
		}
	}
}

func createRace(db *gorm.DB, race Race, abilities []Abilities) {
	err := db.Transaction(func(db *gorm.DB) error {
		var raceTMP Race
		db.Table("races").Find(&raceTMP, "name = ? and type = ? and source = ?", race.Name, race.Type, race.Source)
		if raceTMP.ID != 0 {
			return fmt.Errorf("can't create race '%s', already exists by id = %d", race.Name, raceTMP.ID)
		}
		result := db.Table("races").Create(&race)
		if race.ID == 0 {
			return fmt.Errorf("error, can't create race '%s', err = %s", race.Name, result.Error)
		}
		for _, v := range abilities {
			rAbility := RacesAbilities{
				Race:      race.ID,
				Key:       v.Key,
				Name:      v.Name,
				ShortName: v.ShortName,
				Value:     v.Value,
			}
			result = db.Table("races_abilities").Create(&rAbility)
			if rAbility.ID == 0 {
				return fmt.Errorf("error, can't create race '%s', err = %s", race.Name, result.Error)
			}
		}
		fmt.Printf("Succesfully created race '%s', id = %d\n", race.Name, race.ID)
		return nil
	})
	if err != nil {
		fmt.Println(err)
	}
}

func fillRaces(db *gorm.DB) {
	data := getData("https://ttg.club/api/v1/races", []byte(`{"page":0,"limit":-1,"search":{"value":"","exact":false},"order":[{"field":"name","direction":"asc"}]}`))

	var races []RaceData
	err := json.Unmarshal(data, &races)
	if err != nil {
		fmt.Println("ERR: ", err)
	}

	for _, v := range races {
		race := Race{
			Name:   v.Name.Eng,
			Rus:    v.Name.Rus,
			URL:    v.URL,
			Type:   v.Type.Name,
			Source: v.Source.ShortName,
		}
		abilities := v.Abilities
		createRace(db, race, abilities)
		for _, sub := range v.Subraces {
			subRace := Race{
				Name:   sub.Name.Eng,
				Rus:    sub.Name.Rus,
				URL:    sub.URL,
				Type:   sub.Type.Name,
				Source: sub.Source.ShortName,
			}
			subAbilities := sub.Abilities
			createRace(db, subRace, subAbilities)
		}
	}
}

func fillSchools(db *gorm.DB) {
	var schools = []School{{
		Name: "OTHER",
		Rus:  "особая",
	}, {
		Name: "CONJURATION",
		Rus:  "вызов",
	}, {
		Name: "EVOCATION",
		Rus:  "воплощение",
	}, {
		Name: "ILLUSION",
		Rus:  "иллюзия",
	}, {
		Name: "NECROMANCY",
		Rus:  "некромантия",
	}, {
		Name: "ABJURATION",
		Rus:  "ограждение",
	}, {
		Name: "ENCHANTMENT",
		Rus:  "очарование",
	}, {
		Name: "TRANSMUTATION",
		Rus:  "преобразование",
	}, {
		Name: "DIVINATION",
		Rus:  "прорицание",
	},
	}

	result := db.Table("schools").Create(&schools)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
}

func fillDamageTypes(db *gorm.DB) {
	var damageTypes = []DamageType{{
		Name: "OTHER",
		Rus:  "особый",
	}, {
		Name: "NO_DAMAGE",
		Rus:  "без урона",
	}, {
		Name: "BLUDGEONING",
		Rus:  "дробящий",
	}, {
		Name: "PIERCING",
		Rus:  "колющий",
	}, {
		Name: "SLASHING",
		Rus:  "рубящий",
	}, {
		Name: "FAIR",
		Rus:  "огонь",
	}, {
		Name: "COLD",
		Rus:  "холод",
	}, {
		Name: "LIGHTNING",
		Rus:  "электричество",
	}, {
		Name: "POISON",
		Rus:  "яд",
	}, {
		Name: "ACID",
		Rus:  "кислота",
	}, {
		Name: "SOUND",
		Rus:  "звук",
	}, {
		Name: "NECTOTIC",
		Rus:  "некротическая энергия",
	}, {
		Name: "RADIANT",
		Rus:  "излучение",
	}, {
		Name: "FORCE",
		Rus:  "силовое поле",
	}, {
		Name: "PSYCHIC",
		Rus:  "психическая энергия",
	}}

	result := db.Table("damage_types").Create(&damageTypes)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
}

func fillTimecastTypes(db *gorm.DB) {
	var timecast = []DamageType{{
		Name: "OTHER",
		Rus:  "особая",
	}, {
		Name: "1 BONUS",
		Rus:  "1 бонусное действие",
	}, {
		Name: "1 REACTION",
		Rus:  "1 реакция",
	}, {
		Name: "1 ACTION",
		Rus:  "1 действие",
	}, {
		Name: "1 ROUND",
		Rus:  "1 ход",
	}, {
		Name: "1 MINUTE",
		Rus:  "1 минута",
	}, {
		Name: "10 MINUTE",
		Rus:  "10 минут",
	}, {
		Name: "1 HOUR",
		Rus:  "1 час",
	}, {
		Name: "8 HOUR",
		Rus:  "8 часов",
	}, {
		Name: "12 HOUR",
		Rus:  "12 часов",
	}, {
		Name: "24 HOUR",
		Rus:  "24 часа",
	}}

	result := db.Table("timecast_types").Create(&timecast)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
}

func fillDistanceTypes(db *gorm.DB) {
	var distance = []DistanceType{{
		Name: "особая",
	}, {
		Name: "На себя",
	}, {
		Name: "Касание",
	}, {
		Name: "5 футов",
	}, {
		Name: "10 футов",
	}, {
		Name: "25 футов",
	}, {
		Name: "30 футов",
	}, {
		Name: "40 футов",
	}, {
		Name: "50 футов",
	}, {
		Name: "60 футов",
	}, {
		Name: "90 футов",
	}, {
		Name: "100 футов",
	}, {
		Name: "150 футов",
	}, {
		Name: "300 футов",
	}, {
		Name: "400 футов",
	}, {
		Name: "1000 футов",
	}, {
		Name: "1 миля",
	}, {
		Name: "500 миль",
	}}

	result := db.Table("distance_types").Create(&distance)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
}

func fillDurationTypes(db *gorm.DB) {
	var duration = []DurationType{{
		Name: "особая",
	}, {
		Name: "Мгновенная",
	}, {
		Name: "1 раунд",
	}, {
		Name: "1 минута",
	}, {
		Name: "10 минут",
	}, {
		Name: "1 час",
	}, {
		Name: "8 часов",
	}, {
		Name: "12 часов",
	}, {
		Name: "24 часа",
	}, {
		Name: "1 день",
	}, {
		Name: "7 дней",
	}, {
		Name: "10 дней",
	}, {
		Name: "1 год",
	}}

	result := db.Table("duration_types").Create(&duration)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
}

func defineDurationType(durationStr string, durations []DurationType) DurationType {
	for _, duration := range durations {
		if len(duration.Name) > 2 {
			durationCheck := regexp.MustCompile(duration.Name[:len(duration.Name)-2])
			if durationCheck.MatchString(durationStr) {
				return duration
			}
		}
	}
	return DurationType{ID: 1}
}

func defineTimecastType(timecastStr string, timecasts []TimecastType) TimecastType {
	for _, timecast := range timecasts {
		if len(timecast.Name) > 2 {
			timecastCheck := regexp.MustCompile(timecast.Rus[:len(timecast.Rus)-2])
			// fmt.Println("TIMECASTDEF: ", timecast.Rus[:len(timecast.Rus)-2], ", str: ", timecastStr)
			if timecastCheck.MatchString(timecastStr) {
				return timecast
			}
		}
	}
	return TimecastType{ID: 1}
}

func defineDistanceType(distancetStr string, distances []DistanceType) DistanceType {
	for _, distance := range distances {
		if len(distance.Name) > 2 {
			distanceCheck := regexp.MustCompile(distance.Name[:len(distance.Name)-2])
			// fmt.Println("TIMECASTDEF: ", timecast.Rus[:len(timecast.Rus)-2], ", str: ", timecastStr)
			if distanceCheck.MatchString(distancetStr) {
				return distance
			}
		}
	}
	return DistanceType{ID: 1}
}

func parseMaterial(m string) Material {
	var material Material

	material.Text = m

	reExp := regexp.MustCompile(`расход`)
	if reExp.MatchString(m) {
		material.Expendable = true
	}

	costIndex := 0
	reCost := regexp.MustCompile(`(\d[\d\s]*[мсзпэ]м)`)
	for _, cost := range reCost.FindAllString(m, -1) {
		copper := regexp.MustCompile("мм")
		silver := regexp.MustCompile("см")
		gold := regexp.MustCompile("зм")
		platina := regexp.MustCompile("пм")
		electrum := regexp.MustCompile("эм")

		switch {
		case copper.MatchString(cost):
			costIndex = 1
		case silver.MatchString(cost):
			costIndex = 10
		case electrum.MatchString(cost):
			costIndex = 50
		case gold.MatchString(cost):
			costIndex = 100
		case platina.MatchString(cost):
			costIndex = 1000
		}
		c, err := strconv.Atoi(strings.ReplaceAll(cost[:len(cost)-5], " ", ""))
		if err != nil {
			fmt.Println("Error:", err)
		}
		material.Cost += costIndex * c
	}

	return material
}

func fillSpells(db *gorm.DB) {

	var durationTypes []DurationType
	db.Table("duration_types").Find(&durationTypes)
	var timecastTypes []TimecastType
	db.Table("timecast_types").Find(&timecastTypes)
	var distanceTypes []DistanceType
	db.Table("distance_types").Find(&distanceTypes)
	var schoolTypes []School
	db.Table("schools").Find(&schoolTypes)
	var schools = make(map[string]School)
	for _, school := range schoolTypes {
		schools[school.Rus] = school
	}

	for lvl := 0; lvl < 10; lvl++ {
		data := getData("https://ttg.club/api/v1/spells", []byte("{\"page\":0,\"limit\":500,\"search\":{\"value\":\"\",\"exact\":false},\"order\":[{\"field\":\"level\",\"direction\":\"asc\"},{\"field\":\"name\",\"direction\":\"asc\"}],\"filter\":{\"book\":[\"PHB\",\"XGE\",\"TCE\",\"FTD\",\"LLK\",\"AI\",\"IDRotF\",\"SCAG\",\"GGR\",\"EGtW\",\"SCC\",\"AAG\",\"UAMM\",\"UATOBM\",\"UASS\",\"UACDW\",\"UAFRW\",\"UA20POR\",\"UASMT\",\"UA21DO\",\"UA22WotM\",\"PG\",\"MHH\",\"ODL\",\"DMf5E\",\"GHtPG\",\"TDCS\"],\"level\":[\""+fmt.Sprint(lvl)+"\"],\"class\":[],\"school\":[],\"ritual\":[],\"concentration\":[],\"damageType\":[],\"timecast\":[],\"distance\":[],\"duration\":[],\"components\":[]}}"))

		var spellsData []SpellData
		err := json.Unmarshal(data, &spellsData)
		if err != nil {
			fmt.Println("ERR: ", err)
		}

		progress := 0
		for _, v := range spellsData {
			url := v.URL

			data = getData("https://ttg.club/api/v1"+url, []byte(``))
			var uniqueSpellData UiqueSpellData
			err := json.Unmarshal(data, &uniqueSpellData)
			if err != nil {
				fmt.Println("ERR: ", err)
			}
			progress++
			fmt.Printf("\r[%d/%d] level: %d", progress, len(spellsData), lvl)
			var durationType = defineDurationType(uniqueSpellData.Duration, durationTypes)
			var timecastType = defineTimecastType(uniqueSpellData.Time, timecastTypes)
			var distanceType = defineDistanceType(uniqueSpellData.Range, distanceTypes)

			// fmt.Print(amount, " - Level: "+strconv.Itoa(uniqueSpellData.Level)+", Name: "+uniqueSpellData.Name.Rus)
			// fmt.Print(", Duration: ", durationType.Name, ", Duration full: ", uniqueSpellData.Duration)
			// fmt.Print(", Timecast: ", timecastType.Rus, ", Timecast full: ", uniqueSpellData.Time)
			// fmt.Print(", Distance: ", distanceType.Name, ", Distance full: ", uniqueSpellData.Range)
			// fmt.Print("\n")

			spell := Spell{
				Name:          v.Name.Eng,
				Rus:           v.Name.Rus,
				Level:         v.Level,
				School:        schools[uniqueSpellData.School].ID,
				Ritual:        uniqueSpellData.Ritual,
				Concentration: uniqueSpellData.Сoncentration,
				Duration:      durationType.ID,
				DurationFull:  uniqueSpellData.Duration,
				Timecast:      timecastType.ID,
				TimecastFull:  uniqueSpellData.Time,
				Description:   uniqueSpellData.Description,
				Verbal:        uniqueSpellData.Components.V,
				Somatic:       uniqueSpellData.Components.S,
				Distance:      distanceType.ID,
				DistanceFull:  uniqueSpellData.Range,
				Url:           uniqueSpellData.URL,
			}
			if len(uniqueSpellData.Upper) > 0 {
				spell.Upper = uniqueSpellData.Upper
			}

			var spellCheck Spell
			db.Table("spells").Find(&spellCheck, "name = ?", spell.Name)
			if spellCheck.ID == 0 {
				var material Material
				db.Table("materials").Find(&material, "text = ?", uniqueSpellData.Components.M)

				if material.ID == 0 {
					material = parseMaterial(uniqueSpellData.Components.M)
					db.Table("materials").Create(&material)
				}
				spell.Material = material.ID
				db.Table("spells").Create(&spell)
				fillSpellClasses(db, spell)
			}
		}
		fmt.Print(("\n"))
	}
}

func fillSpellsByDamage(db *gorm.DB) {
	var damageTypes []DamageType
	db.Table("damage_types").Find(&damageTypes)

	for lvl := 0; lvl < 10; lvl++ {
		for _, damage := range damageTypes {
			if damage.ID == 1 {
				continue
			}
			data := getData("https://ttg.club/api/v1/spells", []byte("{\"page\":0,\"limit\":500,\"search\":{\"value\":\"\",\"exact\":false},\"order\":[{\"field\":\"level\",\"direction\":\"asc\"},{\"field\":\"name\",\"direction\":\"asc\"}],\"filter\":{\"book\":[\"PHB\",\"XGE\",\"TCE\",\"FTD\",\"LLK\",\"AI\",\"IDRotF\",\"SCAG\",\"GGR\",\"EGtW\",\"SCC\",\"AAG\",\"UAMM\",\"UATOBM\",\"UASS\",\"UACDW\",\"UAFRW\",\"UA20POR\",\"UASMT\",\"UA21DO\",\"UA22WotM\",\"PG\",\"MHH\",\"ODL\",\"DMf5E\",\"GHtPG\",\"TDCS\"],\"level\":[\""+fmt.Sprint(lvl)+"\"],\"class\":[],\"school\":[],\"ritual\":[],\"concentration\":[],\"damageType\":[\""+damage.Name+"\"],\"timecast\":[],\"distance\":[],\"duration\":[],\"components\":[]}}"))
			// data := getData("https://ttg.club/api/v1/spells", []byte("{\"page\":0,\"limit\":500,\"search\":{\"value\":\"\",\"exact\":false},\"order\":[{\"field\":\"level\",\"direction\":\"asc\"},{\"field\":\"name\",\"direction\":\"asc\"}],\"filter\":{\"book\":[\"PHB\",\"XGE\",\"TCE\",\"FTD\",\"LLK\",\"AI\",\"IDRotF\",\"SCAG\",\"GGR\",\"EGtW\",\"SCC\",\"AAG\",\"UAMM\",\"UATOBM\",\"UASS\",\"UACDW\",\"UAFRW\",\"UA20POR\",\"UASMT\",\"UA21DO\",\"UA22WotM\",\"PG\",\"MHH\",\"ODL\",\"DMf5E\",\"GHtPG\",\"TDCS\"],\"level\":[\""+fmt.Sprint(lvl)+"\"],\"class\":[],\"school\":[],\"ritual\":[],\"concentration\":[],\"damageType\":[],\"timecast\":[],\"distance\":[],\"duration\":[],\"components\":[]}}"))

			var spellsData []SpellData
			err := json.Unmarshal(data, &spellsData)
			if err != nil {
				fmt.Println("ERR: ", err)
			}

			for _, v := range spellsData {
				var spell Spell
				result := db.Table("spells").Find(&spell, "name = ?", v.Name.Eng)
				if spell.ID == 0 {
					fmt.Println("Can not find spell -", v.Name.Eng, ", error -", result.Error)
					continue
				}
				if spell.Damage != 0 {
					continue
				}
				// fmt.Println("ID: ", spell.ID, "Name: "+spell.Name+", RowsAffected:", result.RowsAffected, "ERROR", result.Error)
				spell.Damage = damage.ID
				result = db.Model(&spell).Updates(spell)
				// fmt.Println("ID: ", spell.ID, "Name: "+spell.Name+", RowsAffected:", result.RowsAffected, "ERROR", result.Error)
			}
		}
	}
}

func fillMissingDamage(db *gorm.DB, path string) {
	file, err := os.Open(path)
	if err != nil {
		fmt.Println("ERR: ", err)
	}
	defer file.Close()

	count := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		count++
		sql := scanner.Text()
		fmt.Printf("[%d] sql: %s", count, sql)
		result := db.Table("spells").Exec(sql)
		fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
	}
}

func fillSpellClasses(db *gorm.DB, spell Spell) {
	db.Table("spells").Find(&spell, "name = ?", spell.Name)

	url := spell.Url
	data := getData("https://ttg.club/api/v1"+url, []byte(``))
	var uniqueSpellData UiqueSpellData
	err := json.Unmarshal(data, &uniqueSpellData)
	if err != nil {
		fmt.Println("ERR: ", err)
	}

	for _, classData := range uniqueSpellData.Classes {
		var class Class
		fmt.Println("CLASS=", classData.Name)
		result := db.Table("classes").Find(&class, "rus = ?", classData.Name)
		if result.Error != nil {
			fmt.Println("ERROR", result.Error)
		}
		cs := ClassSpell{Class: class.ID, Spell: spell.ID}
		db.Table("classes_spells").Create(&cs)
	}
}

func proccessSpells(db *gorm.DB) {
	fillSpells(db)
	fillSpellsByDamage(db)
	fillMissingDamage(db, "./misc/damage.sql")
}

func main() {
	db := connectDB()

	fillClases(db)
	fillSchools(db)
	fillDamageTypes(db)
	fillTimecastTypes(db)
	fillDistanceTypes(db)
	fillDurationTypes(db)
	proccessSpells(db)
	fillRaces(db)
}
