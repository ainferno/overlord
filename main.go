package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"

	userHandlers "overlord/handlers/users"

	"github.com/go-playground/validator"
	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"

	"github.com/labstack/echo/v4"
)

var port string
var db *gorm.DB

var validate *validator.Validate
var allowedOrigin string

func connectDB() *gorm.DB {
	dbHost := os.Getenv("DB_HOST")
	dbPort := os.Getenv("DB_PORT")
	dbName := os.Getenv("DB_DATABASE")
	dbUsername := os.Getenv("DB_USERNAME")
	dbPassword := os.Getenv("DB_PASSWORD")

	// dsn := fmt.Sprintf("%s:%s@tcp(%s:%s)/%s", dbUsername, dbPassword, dbHost, dbPort, dbName)
	dsn := fmt.Sprintf("host=%s user=%s password=%s dbname=%s port=%s sslmode=%s TimeZone=%s", dbHost, dbUsername, dbPassword, dbName, dbPort, "disable", "Europe/Moscow")
	db, err := gorm.Open(postgres.Open(dsn), &gorm.Config{})

	if err != nil {
		panic("ERROR: db connection error")
	}

	return db
}

func init() {
	godotenv.Load()
	port = fmt.Sprintf(":%s", os.Getenv("PORT"))
	allowedOrigin = os.Getenv("ALLOWED_ORIGIN")
	db = connectDB()
	validate = validator.New()
}

func homepageHandler(c echo.Context) error {
	log.Println("Home Page")
	return c.String(http.StatusOK, "Hello, World!")
}

func setupHandlers(handler *echo.Echo) {
	api := handler.Group("/api")

	userHandlers := userHandlers.New(db, validate)

	api.GET("/users", userHandlers.Index)
	api.Match([]string{"GET", "POST"}, "/users/show", userHandlers.Show)
	api.Match([]string{"GET", "POST"}, "/users/delete", userHandlers.Delete)
	api.Match([]string{"GET", "POST"}, "/users/update", userHandlers.Update)
	api.Match([]string{"GET", "POST"}, "/users/create", userHandlers.Create)
}

func main() {

	echo := echo.New()

	echo.GET("/", homepageHandler)

	setupHandlers(echo)

	server := http.Server{
		Addr:    port,
		Handler: echo,
	}

	go func() {
		log.Println("Starting http server at", port)
		err := server.ListenAndServe()
		if err != nil {
			log.Fatal(err)
		}
	}()

	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt)
	signal.Notify(sigChan, os.Kill)
	sig := <-sigChan
	log.Printf("Recieved terminate signal, graceful shutdown, signal: [%s]", sig)
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	server.Shutdown(ctx)

	// sm := mux.NewRouter()
	// sm.HandleFunc("/", homepageHandler)

	// api := echo.Group("/api")

	// userHandlers := userHandlers.New(db, validate)

	// characterHandlers := characterHandlers.New(db, validate)
	// apiRouter.HandleFunc("/characters/create", characterHandlers.Create).Methods(http.MethodGet, http.MethodPost)
	// apiRouter.HandleFunc("/characters/list", characterHandlers.Index).Methods(http.MethodGet, http.MethodPost)
	// apiRouter.HandleFunc("/characters/delete", characterHandlers.Delete).Methods(http.MethodGet, http.MethodPost)
	// apiRouter.HandleFunc("/characters/show", characterHandlers.Show).Methods(http.MethodGet, http.MethodPost)
	// // apiRouter.HandleFunc("/characters/update", characterHandlers.Update).Methods(http.MethodPost)
	// // apiRouter.HandleFunc("/characters/{id:[0-9]+}/by_user", characterHandlers.ByUser).Methods(http.MethodPost)

	// spellHandlers := spellHandlers.New(db, validate)
	// apiRouter.HandleFunc("/spells/show", spellHandlers.Show).Methods(http.MethodGet, http.MethodPost)

	// sm.Use(middleware.Logging)

	// cors := gohandlers.CORS(
	// 	gohandlers.AllowedHeaders([]string{"Content-Type"}),
	// 	gohandlers.AllowedOrigins([]string{allowedOrigin}),
	// 	gohandlers.AllowedMethods([]string{"GET", "POST", "PUT", "DELETE", "OPTIONS"}),
	// )

	// server := &http.Server{
	// 	Addr:         port,
	// 	Handler:      cors(sm),
	// 	IdleTimeout:  120 * time.Second,
	// 	ReadTimeout:  1 * time.Second,
	// 	WriteTimeout: 1 * time.Second,
	// }
}
