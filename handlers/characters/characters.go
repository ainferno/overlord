package handlers

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"overlord/models/api"
	"overlord/models/characters"
	"overlord/models/users"
	"strconv"

	"github.com/go-playground/validator"
	"gorm.io/gorm"
)

type Characters struct {
	users      *users.Users
	characters *characters.Characters
	validator  *validator.Validate
}

func New(db *gorm.DB, v *validator.Validate) *Characters {
	characters := characters.New(db)
	users := users.New(db)
	return &Characters{users, characters, v}
}

func (c *Characters) Index(rw http.ResponseWriter, r *http.Request) {
	log.Println("Characters Index Page")

	characters := c.characters.Find()

	var data []api.Character

	for _, c := range characters {
		data = append(data, api.GetCharacter(c))
	}

	rw.Header().Add("Content-Type", "application/json")

	e := json.NewEncoder(rw)
	err := e.Encode(data)
	if err != nil {
		http.Error(rw, "Unable to marshal json", http.StatusInternalServerError)
	}
}

func (c *Characters) Show(rw http.ResponseWriter, r *http.Request) {
	var params map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		panic(err)
	}

	id, err := strconv.Atoi(params["id"].(string))
	if err != nil {
		panic(err)
	}

	data := c.characters.Read(id)

	rw.Header().Add("Content-Type", "application/json")

	e := json.NewEncoder(rw)
	err = e.Encode(api.GetCharacter(data))
	if err != nil {
		http.Error(rw, "Unable to marshal json", http.StatusInternalServerError)
	}
}

func (c *Characters) Create(rw http.ResponseWriter, r *http.Request) {
	var character characters.Character
	err := json.NewDecoder(r.Body).Decode(&character)
	if err != nil {
		http.Error(rw, "Unable to decode user"+err.Error(), http.StatusInternalServerError)
		return
	}

	user := c.users.Read(character.User)
	character.Owner = user

	err = c.validator.Struct(character)
	if err != nil {
		fmt.Println("Error: ", err)
		http.Error(rw, "Validation error", http.StatusUnprocessableEntity)
		return
	}

	character = c.characters.Create(character)

	rw.Header().Add("Content-Type", "application/json")

	e := json.NewEncoder(rw)
	err = e.Encode(api.GetCharacter(character))
	if err != nil {
		http.Error(rw, "Unable to marshal json", http.StatusInternalServerError)
	}
}

func (c *Characters) Delete(rw http.ResponseWriter, r *http.Request) {
	var params map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		panic(err)
	}

	id, err := strconv.Atoi(params["id"].(string))
	if err != nil {
		panic(err)
	}

	c.characters.Delete(id)

	rw.WriteHeader(http.StatusNoContent)
}

// func (c *Characters) Update(rw http.ResponseWriter, r *http.Request) {
// 	vars := mux.Vars(r)

// 	id, err := strconv.Atoi(vars["id"])
// 	if err != nil {
// 		http.Error(rw, err.Error(), http.StatusInternalServerError)
// 	}

// 	var character characters.Character
// 	err = json.NewDecoder(r.Body).Decode(&character)
// 	if err != nil {
// 		http.Error(rw, "Unable to decode user", http.StatusInternalServerError)
// 	}

// 	user := c.users.Read(character.User)
// 	character.Owner = user

// 	err = c.validator.Struct(character)
// 	if err != nil {
// 		http.Error(rw, "Validation error", http.StatusUnprocessableEntity)
// 		return
// 	}

// 	c.characters.Update(character, id)

// 	rw.WriteHeader(http.StatusNoContent)
// }

func (c *Characters) ByUser(rw http.ResponseWriter, r *http.Request) {
	var params map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		panic(err)
	}

	id, err := strconv.Atoi(params["id"].(string))
	if err != nil {
		panic(err)
	}

	characters := c.characters.GetUserCharacters(id)

	var data []api.Character

	for _, c := range characters {
		data = append(data, api.GetCharacter(c))
	}

	rw.Header().Add("Content-Type", "application/json")

	e := json.NewEncoder(rw)
	err = e.Encode(data)
	if err != nil {
		http.Error(rw, "Unable to marshal json", http.StatusInternalServerError)
	}
}

func (c *Characters) ChangeHP(rw http.ResponseWriter, r *http.Request) {
	var params map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		panic(err)
	}

	id, err := strconv.Atoi(params["id"].(string))
	if err != nil {
		panic(err)
	}

	diff, err := strconv.Atoi(params["diff"].(string))
	if err != nil {
		panic(err)
	}

	character := c.characters.Read(id)
	character.ChangeHP(diff)
	c.characters.Update(character, character.ID)

	rw.Header().Add("Content-Type", "application/json")

	e := json.NewEncoder(rw)
	err = e.Encode(api.GetCharacter(character))
	if err != nil {
		http.Error(rw, "Unable to marshal json", http.StatusInternalServerError)
	}
}
