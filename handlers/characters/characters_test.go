package handlers

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"overlord/models/api"
	"overlord/models/characters"
	"overlord/models/users"
	"testing"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestCharactersIndex(t *testing.T) {
	// создаем mock db
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	// db.AutoMigrate(&users.Users{})
	db.AutoMigrate(&characters.Character{})

	user1 := users.User{Name: "Alice", Email: "alice@example.com", Password: "password"}
	user2 := users.User{Name: "Bob", Email: "bob@example.com", Password: "password"}
	db.Create(&user1)
	db.Create(&user2)

	character1 := characters.Character{
		Name:      "Gandalf",
		User:      1,
		MaxHealth: 100,
		Health:    80,
		Owner:     user1,
	}
	character2 := characters.Character{
		Name:      "Aragorn",
		User:      2,
		MaxHealth: 120,
		Health:    120,
		Owner:     user2,
	}
	db.Create(&character1)
	db.Create(&character2)

	// создаем mock валидатор
	v := validator.New()

	// создаем обработчик и роутер
	c := New(db, v)
	router := mux.NewRouter()
	router.HandleFunc("/api/characters", c.Index)

	// создаем запрос на получение списка пользователей
	req, _ := http.NewRequest("GET", "/api/characters", nil)
	rec := httptest.NewRecorder()
	router.ServeHTTP(rec, req)

	// проверяем статус-код и ответ
	assert.Equal(t, http.StatusOK, rec.Code)

	var data []api.Character
	err := json.Unmarshal(rec.Body.Bytes(), &data)

	assert.Nil(t, err)
	assert.Equal(t, 2, len(data))

	assert.Equal(t, "Gandalf", data[0].Name)
	assert.Equal(t, 100, data[0].MaxHealth)
	assert.Equal(t, 80, data[0].Health)
	assert.Equal(t, api.GetUser(user1), data[0].User)

	assert.Equal(t, "Aragorn", data[1].Name)
	assert.Equal(t, 120, data[1].MaxHealth)
	assert.Equal(t, 120, data[1].Health)
	assert.Equal(t, api.GetUser(user2), data[1].User)

	// очищаем базу данных
	if err := db.Migrator().DropTable(&characters.Character{}); err != nil {
		t.Fatal(err)
	}
}

func TestCharactersShow(t *testing.T) {
	// создаем mock db
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	// db.AutoMigrate(&users.Users{})
	db.AutoMigrate(&characters.Character{})

	user1 := users.User{Name: "Alice", Email: "alice@example.com", Password: "password"}
	db.Create(&user1)

	character1 := characters.Character{
		Name:      "Gandalf",
		User:      1,
		MaxHealth: 100,
		Health:    80,
		Owner:     user1,
	}
	db.Create(&character1)

	// создаем mock валидатор
	v := validator.New()

	// создаем обработчик и роутер
	c := New(db, v)
	router := mux.NewRouter()
	router.HandleFunc("/api/characters/{id}", c.Show)

	// создаем запрос на получение пользователя
	req, _ := http.NewRequest("GET", "/api/characters/1", nil)
	rec := httptest.NewRecorder()
	router.ServeHTTP(rec, req)

	// проверяем статус-код и ответ
	assert.Equal(t, http.StatusOK, rec.Code)

	var data api.Character
	err := json.Unmarshal(rec.Body.Bytes(), &data)

	assert.Nil(t, err)

	assert.Equal(t, "Gandalf", data.Name)
	assert.Equal(t, 100, data.MaxHealth)
	assert.Equal(t, 80, data.Health)
	assert.Equal(t, api.GetUser(user1), data.User)

	// очищаем базу данных
	if err := db.Migrator().DropTable(&characters.Character{}); err != nil {
		t.Fatal(err)
	}
}

// func TestUsersCreate(t *testing.T) {
// 	// создаем mock db
// 	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
// 	db.AutoMigrate(&characters.Character{})

// 	user := users.User{Name: "Alice", Email: "alice@example.com", Password: "password"}
// 	db.Create(&user)

// 	character := characters.Character{
// 		Name:      "Gandalf",
// 		User:      1,
// 		MaxHealth: 100,
// 		Health:    80,
// 		Owner:     user,
// 	}

// 	// создаем mock валидатор
// 	v := validator.New()

// 	// создаем обработчик и роутер
// 	c := New(db, v)
// 	router := mux.NewRouter()
// 	router.HandleFunc("/api/characters", c.Create)

// 	// создаем запрос на создание пользователя
// 	body, _ := json.Marshal(character)
// 	req, _ := http.NewRequest("POST", "/api/characters", bytes.NewBuffer(body))
// 	rec := httptest.NewRecorder()
// 	router.ServeHTTP(rec, req)

// 	// проверяем статус-код и ответ
// 	assert.Equal(t, http.StatusOK, rec.Code)

// 	var data api.Character
// 	err := json.Unmarshal(rec.Body.Bytes(), &data)

// 	assert.Nil(t, err)

// 	assert.Equal(t, "Gandalf", data.Name)
// 	assert.Equal(t, 100, data.MaxHealth)
// 	assert.Equal(t, 80, data.Health)
// 	assert.Equal(t, api.GetUser(user), data.User)

// 	// очищаем базу данных
// 	if err := db.Migrator().DropTable(&characters.Character{}); err != nil {
// 		t.Fatal(err)
// 	}
// }

// func TestUsersUpdate(t *testing.T) {
// 	// создаем mock db
// 	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
// 	db.AutoMigrate(&users.User{})
// 	user := users.User{Name: "Alice", Email: "alice@example.com", Password: "password"}
// 	db.Create(&user)

// 	// создаем mock валидатор
// 	v := validator.New()

// 	// создаем обработчик и роутер
// 	u := New(db, v)
// 	router := mux.NewRouter()
// 	router.HandleFunc("/api/users/{id}", u.Update)

// 	// создаем запрос на обновление пользователя
// 	user.Name = "Bob"
// 	body, _ := json.Marshal(user)
// 	req, _ := http.NewRequest("PUT", "/api/users/1", bytes.NewBuffer(body))
// 	rec := httptest.NewRecorder()
// 	router.ServeHTTP(rec, req)

// 	// проверяем статус-код
// 	assert.Equal(t, http.StatusNoContent, rec.Code)

// 	// проверяем, что пользователь был обновлен в базе данных
// 	var updatedUser users.User
// 	db.First(&updatedUser, 1)
// 	assert.Equal(t, "Bob", updatedUser.Name)
// 	assert.Equal(t, "alice@example.com", updatedUser.Email)
// 	assert.Nil(t, bcrypt.CompareHashAndPassword([]byte(updatedUser.Password), []byte("password")))
// }

// func TestCharactersDelete(t *testing.T) {
// 	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
// 	db.AutoMigrate(&users.User{}, &characters.Character{})

// 	v := validator.New()

// 	c := New(db, v)

// 	// создаем пользователя
// 	user := users.User{Name: "Alice", Email: "alice@example.com", Password: "password"}
// 	db.Create(&user)

// 	// создаем персонажа
// 	character := characters.Character{Name: "Bob", User: user.ID}
// 	db.Create(&character)

// 	// создаем запрос на удаление персонажа
// 	req, _ := http.NewRequest("DELETE", "/characters/1", nil)
// 	rec := httptest.NewRecorder()
// 	c.Delete(rec, req)

// 	// проверяем статус-код
// 	assert.Equal(t, http.StatusNoContent, rec.Code)

// 	// проверяем, что персонаж был удален из базы данных
// 	var deletedCharacter characters.Character
// 	db.Preload("Owner").First(&deletedCharacter, 1)
// 	assert.Equal(t, characters.Character{}, deletedCharacter)
// }
