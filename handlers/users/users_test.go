package handlers

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/go-playground/validator"
	"github.com/gorilla/mux"
	"github.com/stretchr/testify/assert"
	"golang.org/x/crypto/bcrypt"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"overlord/models/api"
	"overlord/models/users"
)

func TestUsersIndex(t *testing.T) {
	// создаем mock db
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.AutoMigrate(&users.User{})
	user1 := users.User{Name: "Alice", Email: "alice@example.com", Password: "password"}
	user2 := users.User{Name: "Bob", Email: "bob@example.com", Password: "password"}
	db.Create(&user1)
	db.Create(&user2)

	// создаем mock валидатор
	v := validator.New()

	// создаем обработчик и роутер
	u := New(db, v)
	router := mux.NewRouter()
	router.HandleFunc("/api/users", u.Index)

	// создаем запрос на получение списка пользователей
	req, _ := http.NewRequest("GET", "/api/users", nil)
	rec := httptest.NewRecorder()
	router.ServeHTTP(rec, req)

	// проверяем статус-код и ответ
	assert.Equal(t, http.StatusOK, rec.Code)

	var data []api.User
	err := json.Unmarshal(rec.Body.Bytes(), &data)
	assert.Nil(t, err)
	assert.Equal(t, 2, len(data))
	assert.Equal(t, "Alice", data[0].Name)
	assert.Equal(t, "alice@example.com", data[0].Email)
	assert.Equal(t, "Bob", data[1].Name)
	assert.Equal(t, "bob@example.com", data[1].Email)
}

func TestUsersShow(t *testing.T) {
	// создаем mock db
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.AutoMigrate(&users.User{})
	user := users.User{Name: "Alice", Email: "alice@example.com", Password: "password"}
	db.Create(&user)

	// создаем mock валидатор
	v := validator.New()

	// создаем обработчик и роутер
	u := New(db, v)
	router := mux.NewRouter()
	router.HandleFunc("/api/users/{id}", u.Show)

	// создаем запрос на получение пользователя
	req, _ := http.NewRequest("GET", "/api/users/1", nil)
	rec := httptest.NewRecorder()
	router.ServeHTTP(rec, req)

	// проверяем статус-код и ответ
	assert.Equal(t, http.StatusOK, rec.Code)

	var data api.User
	err := json.Unmarshal(rec.Body.Bytes(), &data)
	assert.Nil(t, err)
	assert.Equal(t, "Alice", data.Name)
	assert.Equal(t, "alice@example.com", data.Email)
}

func TestUsersCreate(t *testing.T) {
	// создаем mock db
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.AutoMigrate(&users.User{})

	// создаем mock валидатор
	v := validator.New()

	// создаем обработчик и роутер
	u := New(db, v)
	router := mux.NewRouter()
	router.HandleFunc("/api/users", u.Create)

	// создаем запрос на создание пользователя
	user := users.User{Name: "Alice", Email: "alice@example.com", Password: "password"}
	body, _ := json.Marshal(user)
	req, _ := http.NewRequest("POST", "/api/users", bytes.NewBuffer(body))
	rec := httptest.NewRecorder()
	router.ServeHTTP(rec, req)

	// проверяем статус-код и ответ
	assert.Equal(t, http.StatusOK, rec.Code)

	var data api.User
	err := json.Unmarshal(rec.Body.Bytes(), &data)
	assert.Nil(t, err)
	assert.Equal(t, "Alice", data.Name)
	assert.Equal(t, "alice@example.com", data.Email)

	var updatedUser users.User
	db.First(&updatedUser, data.ID)

	assert.NotNil(t, updatedUser.ID)
	assert.Equal(t, "Alice", updatedUser.Name)
	assert.Equal(t, "alice@example.com", updatedUser.Email)
	assert.Nil(t, bcrypt.CompareHashAndPassword([]byte(updatedUser.Password), []byte("password")))
}

func TestUsersUpdate(t *testing.T) {
	// создаем mock db
	db, _ := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	db.AutoMigrate(&users.User{})
	user := users.User{Name: "Alice", Email: "alice@example.com", Password: "password"}
	db.Create(&user)

	// создаем mock валидатор
	v := validator.New()

	// создаем обработчик и роутер
	u := New(db, v)
	router := mux.NewRouter()
	router.HandleFunc("/api/users/{id}", u.Update)

	// создаем запрос на обновление пользователя
	user.Name = "Bob"
	body, _ := json.Marshal(user)
	req, _ := http.NewRequest("PUT", "/api/users/1", bytes.NewBuffer(body))
	rec := httptest.NewRecorder()
	router.ServeHTTP(rec, req)

	// проверяем статус-код
	assert.Equal(t, http.StatusNoContent, rec.Code)

	// проверяем, что пользователь был обновлен в базе данных
	var updatedUser users.User
	db.First(&updatedUser, 1)
	assert.Equal(t, "Bob", updatedUser.Name)
	assert.Equal(t, "alice@example.com", updatedUser.Email)
	assert.Nil(t, bcrypt.CompareHashAndPassword([]byte(updatedUser.Password), []byte("password")))
}
