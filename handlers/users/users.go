package handlers

import (
	"net/http"

	"overlord/models/api"
	"overlord/models/users"

	"github.com/go-playground/validator"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"gorm.io/gorm"
)

type Users struct {
	users     *users.Users
	validator *validator.Validate
}

type Params struct {
	ID       int    `param:"id" query:"id" form:"id" json:"id" xml:"id"`
	Name     string `param:"name" query:"name" form:"name" json:"name" xml:"name"`
	Email    string `param:"email" query:"email" form:"email" json:"email" xml:"email"`
	Password string `param:"password" query:"password" form:"password" json:"password" xml:"password"`
}

func New(db *gorm.DB, v *validator.Validate) *Users {
	users := users.New(db)
	return &Users{users, v}
}

func (u *Users) Index(c echo.Context) error {
	log.Print("users.Index requested")

	users := u.users.Find()
	var data []api.User

	for _, u := range users {
		data = append(data, api.GetUser(u))
	}

	return c.JSON(http.StatusOK, data)
}

func (u *Users) Show(c echo.Context) error {
	params := new(Params)
	if err := c.Bind(params); err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	data := u.users.Read(params.ID)
	return c.JSON(http.StatusOK, data)
}

func (u *Users) Create(c echo.Context) error {
	params := new(Params)
	if err := c.Bind(params); err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	user := users.User{
		Name:     params.Name,
		Email:    params.Email,
		Password: params.Password,
	}

	err := u.validator.Struct(user)
	if err != nil {
		return c.String(http.StatusUnprocessableEntity, "validation error")
	}
	user = u.users.Create(user)

	return c.JSON(http.StatusOK, api.GetUser(user))
}

func (u *Users) Delete(c echo.Context) error {
	params := new(Params)
	if err := c.Bind(params); err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	u.users.Delete(params.ID)

	return c.NoContent(http.StatusOK)
}

func (u *Users) Update(c echo.Context) error {
	params := new(Params)
	if err := c.Bind(params); err != nil {
		return c.String(http.StatusBadRequest, "bad request")
	}

	user := users.User{
		Name:     params.Name,
		Email:    params.Email,
		Password: params.Password,
	}

	var err error
	if user.Name != "" {
		err = u.validator.StructPartial(user, "Name")
	}
	if user.Email != "" {
		err = u.validator.StructPartial(user, "Email")
	}
	if user.Password != "" {
		err = u.validator.StructPartial(user, "Password")
	}
	if err != nil {
		return c.String(http.StatusUnprocessableEntity, "validation error")
	}

	u.users.Update(user, params.ID)

	return c.NoContent(http.StatusOK)
}
