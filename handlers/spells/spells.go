package handlers

import (
	"encoding/json"
	"net/http"
	"strconv"

	"overlord/models/api"
	"overlord/models/spells"

	"github.com/go-playground/validator"
	"gorm.io/gorm"
)

type Spells struct {
	spells    *spells.Spells
	validator *validator.Validate
}

func New(db *gorm.DB, v *validator.Validate) *Spells {
	spells := spells.New(db)
	return &Spells{spells, v}
}

// func (u *Spells) Index(rw http.ResponseWriter, r *http.Request) {
// 	log.Println("Users Index Page")

// 	users := u.users.Find()
// 	var data []api.User

// 	for _, u := range users {
// 		data = append(data, api.GetUser(u))
// 	}

// 	rw.Header().Add("Content-Type", "application/json")

// 	e := json.NewEncoder(rw)
// 	err := e.Encode(data)
// 	if err != nil {
// 		http.Error(rw, "Unable to marshal json", http.StatusInternalServerError)
// 	}
// }

func (s *Spells) Show(rw http.ResponseWriter, r *http.Request) {
	var params map[string]interface{}
	err := json.NewDecoder(r.Body).Decode(&params)
	if err != nil {
		panic(err.Error())
	}

	id, err := strconv.Atoi(params["id"].(string))
	if err != nil {
		panic(err.Error())
	}
	data := s.spells.Read(id)

	rw.Header().Add("Content-Type", "application/json")

	e := json.NewEncoder(rw)
	err = e.Encode(api.GetSpell(data))
	if err != nil {
		http.Error(rw, "Unable to marshal json", http.StatusInternalServerError)
	}
}

// func (u *Spells) Create(rw http.ResponseWriter, r *http.Request) {
// 	var user users.User
// 	err := json.NewDecoder(r.Body).Decode(&user)
// 	if err != nil {
// 		http.Error(rw, "Unable to decode user"+err.Error(), http.StatusInternalServerError)
// 		return
// 	}
// 	err = u.validator.Struct(user)
// 	if err != nil {
// 		http.Error(rw, "Validation error", http.StatusUnprocessableEntity)
// 		return
// 	}
// 	user = u.users.Create(user)

// 	rw.Header().Add("Content-Type", "application/json")
// 	e := json.NewEncoder(rw)
// 	err = e.Encode(api.GetUser(user))
// 	if err != nil {
// 		http.Error(rw, "Unable to marshal json", http.StatusInternalServerError)
// 	}
// }

// func (u *Spells) Delete(rw http.ResponseWriter, r *http.Request) {
// 	vars := mux.Vars(r)

// 	id, err := strconv.Atoi(vars["id"])
// 	if err != nil {
// 		panic(err)
// 	}

// 	u.users.Delete(id)

// 	rw.WriteHeader(http.StatusNoContent)
// }

// func (u *Spells) Update(rw http.ResponseWriter, r *http.Request) {
// 	vars := mux.Vars(r)

// 	id, err := strconv.Atoi(vars["id"])
// 	if err != nil {
// 		panic(err)
// 	}

// 	var user users.User
// 	err = json.NewDecoder(r.Body).Decode(&user)
// 	if err != nil {
// 		http.Error(rw, "Unable to decode user", http.StatusInternalServerError)
// 	}

// 	err = u.validator.Struct(user)
// 	if err != nil {
// 		http.Error(rw, "Validation error", http.StatusUnprocessableEntity)
// 		return
// 	}

// 	u.users.Update(user, id)

// 	rw.WriteHeader(http.StatusNoContent)
// }
