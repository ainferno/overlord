package races

import (
	"fmt"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type Race struct {
	ID     int
	Name   string
	Rus    string
	URL    string
	Type   string
	Source string
}

type Races struct {
	db *gorm.DB
}

func New(db *gorm.DB) *Races {
	return &Races{db}
}

func (s Races) Find() []Race {
	var races []Race
	result := s.db.Preload(clause.Associations).Find(&races)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
	return races
}

func (s Races) Read(id int) Race {
	var race Race
	result := s.db.Preload(clause.Associations).First(&race, id)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
	return race
}

// func (s Spells) GetCharacterSpells(id int) Spell {
// 	var spells []Spell
// 	result := s.db.Preload(clause.Associations).Find(&spells, "\"user\" = ?", id)
// 	mt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
// 	return spells
// }

// func (c Characters) Create(character Character) Character {
// 	result := c.db.Create(&character)
// 	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
// 	return character
// }

// func (c Characters) Update(character Character, id int) error {
// 	var tmp Character
// 	result := c.db.First(&tmp, id)
// 	if result.Error != nil {
// 		return result.Error
// 	}

// 	character.ID = id
// 	result = c.db.Model(&character).Updates(character)
// 	return result.Error
// }

// func (c Characters) Delete(id int) {
// 	result := c.db.Delete(&Character{}, id)
// 	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
// }

// func (c Characters) GetUserCharacters(id int) []Character {
// 	var characters []Character
// 	result := c.db.Preload("Owner").Find(&characters, "\"user\" = ?", id)
// 	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
// 	return characters
// }
