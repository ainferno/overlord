package api

import (
	"overlord/models/characters"
	"overlord/models/races"
	"overlord/models/spells"
	"overlord/models/users"
)

type User struct {
	ID    int    `json:"id"`
	Name  string `json:"name" validate:"required"`
	Email string `json:"email" validate:"required,email"`
}

func GetUser(user users.User) User {
	return User{
		ID:    user.ID,
		Name:  user.Name,
		Email: user.Email,
	}
}

type Spell struct {
	ID int `json:"id"`

	Name string `json:"name_eng"`
	Rus  string `json:"name_rus"`

	Level int `json:"level"`

	Ritual        bool `json:"ritual"`
	Concentration bool `json:"concentration"`

	School   spells.School       `json:"school"`
	Damage   spells.DamageType   `json:"damage"`
	Timecast spells.TimecastType `json:"timecast"`
	Distance spells.DistanceType `json:"distance"`
	Duration spells.DurationType `json:"duration"`

	TimecastFull string `json:"timecast_full"`
	DistanceFull string `json:"distance_full"`
	DurationFull string `json:"duration_full"`

	Verbal   bool            `json:"verbal"`
	Somatic  bool            `json:"somatic"`
	Material spells.Material `json:"material"`

	Description string `json:"description"`
	Upper       string `json:"upper"`
	Url         string `json:"url"`

	Prepared bool `json:"prepared"`
}

func GetSpell(spell spells.Spell) Spell {
	return Spell{
		ID:            spell.ID,
		Name:          spell.Name,
		Rus:           spell.Rus,
		Level:         spell.Level,
		Ritual:        spell.Ritual,
		Concentration: spell.Concentration,
		School:        spell.SchoolData,
		Damage:        spell.DamageData,
		Timecast:      spell.TimecastData,
		Distance:      spell.DistanceData,
		Duration:      spell.DurationData,
		TimecastFull:  spell.TimecastFull,
		DistanceFull:  spell.DistanceFull,
		DurationFull:  spell.DurationFull,
		Verbal:        spell.Verbal,
		Somatic:       spell.Somatic,
		Material:      spell.MaterialData,
		Description:   spell.Description,
		Upper:         spell.Upper,
		Url:           spell.Url,
	}
}

type Race struct {
	ID     int    `json:"id"`
	Name   string `json:"name_eng"`
	Rus    string `json:"name_rus"`
	URL    string `json:"url"`
	Type   string `json:"type"`
	Source string `json:"source"`
}

func GetRace(race races.Race) Race {
	return Race{
		ID:     race.ID,
		Name:   race.Name,
		Rus:    race.Rus,
		Type:   race.Type,
		Source: race.Source,
		URL:    race.URL,
	}
}

type Stats struct {
	Constitution            int `json:"con"`
	Strength                int `json:"str"`
	Dexterity               int `json:"dex"`
	Charisma                int `json:"cha"`
	Wizdom                  int `json:"wiz"`
	Intelligence            int `json:"int"`
	ConstitutionModificator int `json:"con_mod"`
	StrengthModificator     int `json:"str_mod"`
	DexterityModificator    int `json:"dex_mod"`
	CharismaModificator     int `json:"cha_mod"`
	WizdomModificator       int `json:"wiz_mod"`
	IntelligenceModificator int `json:"int_mod"`
}

type Skills struct {
	Athletics            int `json:"athletics"`
	Acrobatics           int `json:"acrobatics"`
	SleightOfHand        int `json:"sleight_of_hand"`
	Stealth              int `json:"stealth"`
	Arcana               int `json:"arcana"`
	History              int `json:"history"`
	Investigation        int `json:"investigation"`
	Nature               int `json:"nature"`
	Religion             int `json:"religion"`
	AnimalHandling       int `json:"animal_handling"`
	Insight              int `json:"insight"`
	Medicine             int `json:"medicine"`
	Perception           int `json:"perception"`
	Survival             int `json:"survival"`
	Deception            int `json:"deception"`
	Intimidation         int `json:"intimidation"`
	Perfomance           int `json:"perfomance"`
	Persuation           int `json:"persuation"`
	PassivePercention    int `json:"passive_perception"`
	PassiveInvestigation int `json:"passive_investigation"`
}

type Class struct {
	ID   int    `json:"id"`
	Name string `json:"name_eng"`
	Rus  string `json:"name_rus"`
	Dice int    `json:"dice"`
}

type ClassArchetype struct {
	ID    int    `json:"id"`
	Class Class  `json:"class"`
	Name  string `json:"name_eng"`
	Rus   string `json:"name_rus"`
	Level int    `json:"level"`
}

type CharacterSpell struct {
	Character int
	Spell     int
	SpellData spells.Spell `gorm:"foreignKey:Spell"`
	Prepared  bool
}

type Character struct {
	ID              int              `json:"id"`
	Name            string           `json:"name"`
	MaxHealth       int              `json:"max_health"`
	Health          int              `json:"health"`
	Level           int              `json:"level"`
	ProficencyBonus int              `json:"proficency_bonus"`
	Status          int              `json:"status"`
	User            User             `json:"user"`
	Race            Race             `json:"race"`
	Spells          []Spell          `json:"spells"`
	Stats           Stats            `json:"stats"`
	Skills          Skills           `json:"skills"`
	Classes         []ClassArchetype `json:"classes"`
}

func GetClasses(classes []characters.CharacterClass) []ClassArchetype {
	var ClassArchetypes []ClassArchetype
	for _, v := range classes {
		ClassArchetypes = append(ClassArchetypes, ClassArchetype{
			ID:    v.ArchetypeData.ID,
			Name:  v.ArchetypeData.Name,
			Rus:   v.ArchetypeData.Rus,
			Level: v.Level,
			Class: Class{
				ID:   v.ArchetypeData.ClassData.ID,
				Name: v.ArchetypeData.ClassData.Name,
				Rus:  v.ArchetypeData.ClassData.Rus,
				Dice: v.ArchetypeData.ClassData.Dice,
			},
		})
	}
	return ClassArchetypes
}

func GetCharacter(character characters.Character) Character {
	var spells []Spell
	for _, v := range character.CharacterSpells {
		spellData := GetSpell(v.SpellData)
		spellData.Prepared = v.Prepared
		spells = append(spells, spellData)
	}

	level := character.Level()
	proficencyBonus := characters.LevelToBonus(level)

	CharacterStats := Stats{
		Constitution:            character.CharacterStats.Constitution,
		Strength:                character.CharacterStats.Strength,
		Dexterity:               character.CharacterStats.Dexterity,
		Charisma:                character.CharacterStats.Charisma,
		Wizdom:                  character.CharacterStats.Wizdom,
		Intelligence:            character.CharacterStats.Intelligence,
		ConstitutionModificator: characters.StatToMod(character.CharacterStats.Constitution),
		StrengthModificator:     characters.StatToMod(character.CharacterStats.Strength),
		DexterityModificator:    characters.StatToMod(character.CharacterStats.Dexterity),
		CharismaModificator:     characters.StatToMod(character.CharacterStats.Charisma),
		WizdomModificator:       characters.StatToMod(character.CharacterStats.Wizdom),
		IntelligenceModificator: characters.StatToMod(character.CharacterStats.Intelligence),
	}

	CharacterSkills := Skills{
		Athletics:      character.CharacterSkills.Athletics*proficencyBonus + CharacterStats.StrengthModificator,
		Acrobatics:     character.CharacterSkills.Acrobatics*proficencyBonus + CharacterStats.DexterityModificator,
		SleightOfHand:  character.CharacterSkills.SleightOfHand*proficencyBonus + CharacterStats.DexterityModificator,
		Stealth:        character.CharacterSkills.Stealth*proficencyBonus + CharacterStats.DexterityModificator,
		Arcana:         character.CharacterSkills.Arcana*proficencyBonus + CharacterStats.IntelligenceModificator,
		History:        character.CharacterSkills.History*proficencyBonus + CharacterStats.IntelligenceModificator,
		Investigation:  character.CharacterSkills.Investigation*proficencyBonus + CharacterStats.IntelligenceModificator,
		Nature:         character.CharacterSkills.Nature*proficencyBonus + CharacterStats.IntelligenceModificator,
		Religion:       character.CharacterSkills.Religion*proficencyBonus + CharacterStats.IntelligenceModificator,
		AnimalHandling: character.CharacterSkills.AnimalHandling*proficencyBonus + CharacterStats.WizdomModificator,
		Insight:        character.CharacterSkills.Insight*proficencyBonus + CharacterStats.WizdomModificator,
		Medicine:       character.CharacterSkills.Medicine*proficencyBonus + CharacterStats.WizdomModificator,
		Perception:     character.CharacterSkills.Perception*proficencyBonus + CharacterStats.WizdomModificator,
		Survival:       character.CharacterSkills.Survival*proficencyBonus + CharacterStats.WizdomModificator,
		Deception:      character.CharacterSkills.Deception*proficencyBonus + CharacterStats.CharismaModificator,
		Intimidation:   character.CharacterSkills.Intimidation*proficencyBonus + CharacterStats.CharismaModificator,
		Perfomance:     character.CharacterSkills.Perfomance*proficencyBonus + CharacterStats.CharismaModificator,
		Persuation:     character.CharacterSkills.Persuation*proficencyBonus + CharacterStats.CharismaModificator,
	}
	CharacterSkills.PassivePercention = CharacterSkills.Perception + 10
	CharacterSkills.PassiveInvestigation = CharacterSkills.Investigation + 10

	return Character{
		ID:              character.ID,
		Name:            character.Name,
		MaxHealth:       character.MaxHealth,
		Health:          character.Health,
		Level:           level,
		ProficencyBonus: proficencyBonus,
		Race:            GetRace(character.RaceData),
		Status:          character.Status,
		Stats:           CharacterStats,
		Skills:          CharacterSkills,
		Classes:         GetClasses(character.CharacterClasses),
		User: GetUser(
			character.Owner,
		),
		Spells: spells,
	}
}
