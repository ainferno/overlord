package api

import (
	"overlord/models/characters"
	"overlord/models/users"
	"reflect"
	"testing"
)

func TestGetUser(t *testing.T) {
	apiUser := User{ID: 1, Name: "Test1", Email: "test@test.com"}
	modelUser := users.User{ID: 1, Name: "Test1", Email: "test@test.com", Password: "testtest1"}
	result := reflect.DeepEqual(apiUser, GetUser(modelUser))
	if !result {
		t.Error("api User and GetUser(model User) are not equal")
	}
}

func TestGetCharacter(t *testing.T) {
	apiCharacter := Character{ID: 1, Name: "Char1", MaxHealth: 10, Health: 5, User: User{ID: 1, Name: "Test1", Email: "test@test.com"}}
	modelCharacter := characters.Character{
		ID:        1,
		Name:      "Char1",
		MaxHealth: 10,
		Health:    5,
		User:      1,
		Owner: users.User{
			ID:       1,
			Name:     "Test1",
			Email:    "test@test.com",
			Password: "testtest1",
		},
	}
	result := reflect.DeepEqual(apiCharacter, GetCharacter(modelCharacter))
	if !result {
		t.Error("api Character and GetCharacter(model Character) are not equal")
	}
}
