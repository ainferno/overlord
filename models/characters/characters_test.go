package characters

import (
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"

	"overlord/models/users"
)

func TestCharacters(t *testing.T) {
	// подключение к тестовой базе данных
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	assert.NoError(t, err)

	// запуск миграций
	err = db.AutoMigrate(&users.User{}, &Character{})
	assert.NoError(t, err)

	// создание нового пользователя
	user1 := users.User{Name: "John", Email: "john@example.com", Password: "password"}
	user2 := users.User{Name: "Bob", Email: "bob@example.com", Password: "password"}
	result := db.Create(&user1)
	assert.NoError(t, result.Error)
	result = db.Create(&user2)
	assert.NoError(t, result.Error)

	// создание нового персонажа
	character1 := Character{
		Name:      "Hero1",
		User:      user1.ID,
		MaxHealth: 100,
		Health:    100,
	}
	character2 := Character{
		Name:      "Hero2",
		User:      user2.ID,
		MaxHealth: 120,
		Health:    30,
	}
	charactersRepo := New(db)

	t.Run("TestCreate", func(t *testing.T) {
		// создание персонажа
		character1 := charactersRepo.Create(character1)
		assert.NotNil(t, character1.ID)
		assert.Equal(t, "Hero1", character1.Name)

		character2 := charactersRepo.Create(character2)
		assert.NotNil(t, character2.ID)
		assert.Equal(t, "Hero2", character2.Name)

	})

	fmt.Printf("ID: %d, Name: %s, MaxHealth: %d, Health: %d, User: %d\n", character1.ID, character1.Name, character1.MaxHealth, character1.Health, character1.User)

	// t.Run("TestFind", func(t *testing.T) {
	// 	// чтение персонажа
	// 	characters := charactersRepo.Find()

	// 	assert.Equal(t, 2, len(characters))
	// 	assert.Equal(t, "Hero", characters[0].Name)
	// 	assert.Equal(t, 100, characters[0].MaxHealth)
	// 	assert.Equal(t, 100, characters[0].Health)
	// 	assert.Equal(t, user1.ID, characters[0].User)
	// 	assert.Equal(t, user1.ID, characters[0].Owner.ID)
	// 	assert.Equal(t, "Hero2", characters[1].Name)
	// 	assert.Equal(t, 120, characters[1].MaxHealth)
	// 	assert.Equal(t, 30, characters[1].Health)
	// 	assert.Equal(t, user2.ID, characters[1].User)
	// 	assert.Equal(t, user2.ID, characters[1].Owner.ID)
	// })

	// t.Run("TestRead", func(t *testing.T) {
	// 	// чтение персонажа
	// 	fmt.Println(character1)
	// 	characterRead := charactersRepo.Read(character1.ID)
	// 	assert.NotNil(t, characterRead.ID)
	// 	assert.Equal(t, "Hero", characterRead.Name)
	// })

	// t.Run("TestUpdate", func(t *testing.T) {
	// 	// обновление персонажа
	// 	character1.Name = "Super Hero"
	// 	err = charactersRepo.Update(character1, character1.ID)
	// 	assert.NoError(t, err)

	// 	characterRead := charactersRepo.Read(character1.ID)
	// 	assert.NotNil(t, characterRead.ID)
	// 	assert.Equal(t, "Super Hero", characterRead.Name)
	// })

	// t.Run("TestDelete", func(t *testing.T) {
	// 	// удаление персонажа
	// 	charactersRepo.Delete(character1.ID)
	// 	characterRead := charactersRepo.Read(character1.ID)
	// 	assert.Empty(t, characterRead.ID)
	// })

	// t.Run("TestGetUserCharacters", func(t *testing.T) {
	// 	// получение персонажей пользователя
	// 	charactersRepo.Create(character1)
	// 	charactersList := charactersRepo.GetUserCharacters(user1.ID)
	// 	assert.Len(t, charactersList, 1)
	// })
}
