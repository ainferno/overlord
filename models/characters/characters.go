package characters

import (
	"fmt"
	"math"
	"overlord/models/races"
	"overlord/models/spells"
	"overlord/models/users"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type Class struct {
	ID   int
	Name string
	Rus  string
	Dice int
}

type ClassArchetype struct {
	ID        int
	Class     int
	Name      string
	Rus       string
	ClassData Class `gorm:"foreignKey:Class"`
}

type CharacterStats struct {
	Character    int
	Constitution int
	Strength     int
	Dexterity    int
	Charisma     int
	Wizdom       int
	Intelligence int
}

type CharacterSkills struct {
	Character      int
	Athletics      int
	Acrobatics     int
	SleightOfHand  int
	Stealth        int
	Arcana         int
	History        int
	Investigation  int
	Nature         int
	Religion       int
	AnimalHandling int
	Insight        int
	Medicine       int
	Perception     int
	Survival       int
	Deception      int
	Intimidation   int
	Perfomance     int
	Persuation     int
}

type CharacterClass struct {
	Character     int
	Class         int
	Level         int
	KnownSpells   int
	ArchetypeData ClassArchetype `gorm:"foreignKey:Class"`
}

type CharacterSpell struct {
	Character int
	Spell     int
	SpellData spells.Spell `gorm:"foreignKey:Spell"`
	Prepared  bool
}

type Character struct {
	ID               int
	Name             string
	User             int
	MaxHealth        int
	Health           int
	Race             int
	ProficencyBonus  int
	Status           int
	db               *gorm.DB
	Owner            users.User       `gorm:"foreignKey:User"`
	RaceData         races.Race       `gorm:"foreignKey:Race"`
	CharacterSpells  []CharacterSpell `gorm:"foreignKey:Character"`
	CharacterStats   CharacterStats   `gorm:"foreignKey:Character"`
	CharacterSkills  CharacterSkills  `gorm:"foreignKey:Character"`
	CharacterClasses []CharacterClass `gorm:"foreignKey:Character"`
}

type Characters struct {
	db *gorm.DB
}

func New(db *gorm.DB) *Characters {
	return &Characters{db}
}

func prepareCharacter(db *gorm.DB) *gorm.DB {
	return db.
		Preload(clause.Associations).
		Preload("CharacterSpells." + clause.Associations).
		Preload("CharacterSpells.SpellData." + clause.Associations).
		Preload("CharacterClasses." + clause.Associations).
		Preload("CharacterClasses.ArchetypeData." + clause.Associations).
		Preload("CharacterClasses.ArchetypeData.ClassData." + clause.Associations)
}

func (c Characters) Find() []Character {
	var characters []Character
	result := prepareCharacter(c.db).Find(&characters)

	fmt.Println("CHAR:", characters)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
	return characters
}

func (c Characters) Read(id int) Character {
	var character Character

	result := prepareCharacter(c.db).First(&character, id)

	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
	character.db = c.db
	return character
}

func (c Characters) Create(character Character) Character {
	result := c.db.Create(&character)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
	character.db = c.db
	return character
}

func (c Characters) Update(character Character, id int) error {
	var tmp Character
	result := c.db.First(&tmp, id)
	if result.Error != nil {
		return result.Error
	}

	character.ID = id
	result = c.db.Model(&character).Updates(character)
	return result.Error
}

func (c Characters) Delete(id int) {
	result := c.db.Delete(&Character{}, id)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
}

func (c Characters) GetUserCharacters(id int) []Character {
	var characters []Character
	result := c.db.Preload("Owner").Preload("CharacterSpells").Preload("Spell").Find(&characters, "\"user\" = ?", id)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
	return characters
}

func (c Character) Level() int {
	level := 0

	for _, v := range c.CharacterClasses {
		level += v.Level
	}

	return level
}

func (c Character) ChangeHP(diff int) int {
	if c.Status == 2 {
		return 2
	}

	c.Health += diff
	if c.Health > c.MaxHealth {
		c.Health = c.MaxHealth
	}
	if c.Health > 0 && c.Status != 0 {
		c.Status = 0
	} else if c.Health < -1*c.MaxHealth {
		c.Status = 2
	} else if c.Health <= 0 && c.Status != 2 {
		c.Status = 1
	}

	return c.Status
}

func StatToMod(stat int) int {
	return int(math.Floor(float64(stat-10) / 2.0))
}

func LevelToBonus(level int) int {
	return int(math.Floor(2 + float64(level-1)/4))
}
