package spells

import (
	"fmt"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type School struct {
	ID   int
	Name string
	Rus  string
}

type DamageType struct {
	ID   int
	Name string
	Rus  string
}

type TimecastType struct {
	ID   int
	Name string
	Rus  string
}

type DistanceType struct {
	ID   int
	Name string
}

type DurationType struct {
	ID   int
	Name string
}

type Material struct {
	ID         int
	Text       string
	Cost       int
	Expendable bool
}

type Spell struct {
	ID            int
	Name          string
	Rus           string
	Level         int
	Ritual        bool
	Concentration bool

	School       int
	SchoolData   School `gorm:"foreignKey:School"`
	Damage       int
	DamageData   DamageType `gorm:"foreignKey:Damage"`
	Timecast     int
	TimecastData TimecastType `gorm:"foreignKey:Timecast"`
	Distance     int
	DistanceData DistanceType `gorm:"foreignKey:Distance"`
	Duration     int
	DurationData DurationType `gorm:"foreignKey:Duration"`

	TimecastFull string
	DistanceFull string
	DurationFull string

	Material     int
	MaterialData Material `gorm:"foreignKey:Material"`

	Verbal      bool
	Somatic     bool
	Description string
	Upper       string
	Url         string
}

type Spells struct {
	db *gorm.DB
}

func New(db *gorm.DB) *Spells {
	return &Spells{db}
}

func (s Spells) Find() []Spell {
	var spells []Spell
	result := s.db.Preload(clause.Associations).Find(&spells)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
	return spells
}

func (s Spells) Read(id int) Spell {
	var spell Spell
	result := s.db.Preload(clause.Associations).First(&spell, id)
	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
	return spell
}

// func (s Spells) GetCharacterSpells(id int) Spell {
// 	var spells []Spell
// 	result := s.db.Preload(clause.Associations).Find(&spells, "\"user\" = ?", id)
// 	mt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
// 	return spells
// }

// func (c Characters) Create(character Character) Character {
// 	result := c.db.Create(&character)
// 	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
// 	return character
// }

// func (c Characters) Update(character Character, id int) error {
// 	var tmp Character
// 	result := c.db.First(&tmp, id)
// 	if result.Error != nil {
// 		return result.Error
// 	}

// 	character.ID = id
// 	result = c.db.Model(&character).Updates(character)
// 	return result.Error
// }

// func (c Characters) Delete(id int) {
// 	result := c.db.Delete(&Character{}, id)
// 	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
// }

// func (c Characters) GetUserCharacters(id int) []Character {
// 	var characters []Character
// 	result := c.db.Preload("Owner").Find(&characters, "\"user\" = ?", id)
// 	fmt.Println("RowsAffected:", result.RowsAffected, "ERROR", result.Error)
// 	return characters
// }
