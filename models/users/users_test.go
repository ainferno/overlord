package users

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

func TestUsers(t *testing.T) {
	// подключение к тестовой базе данных
	db, err := gorm.Open(sqlite.Open("file::memory:?cache=shared"), &gorm.Config{})
	assert.NoError(t, err)

	// запуск миграций
	err = db.AutoMigrate(&User{})
	assert.NoError(t, err)

	// создание нового пользователя
	user := User{Name: "John", Email: "john@example.com", Password: "password"}
	usersRepo := New(db)

	t.Run("TestCreate", func(t *testing.T) {
		// создание пользователя
		user = usersRepo.Create(user)
		assert.NotNil(t, user.ID)
		assert.Equal(t, "John", user.Name)
	})

	t.Run("TestRead", func(t *testing.T) {
		// чтение пользователя
		userRead := usersRepo.Read(user.ID)
		assert.NotNil(t, userRead.ID)
		assert.Equal(t, "John", userRead.Name)
	})

	t.Run("TestUpdate", func(t *testing.T) {
		// обновление пользователя
		user.Name = "Super John"
		err = usersRepo.Update(user, user.ID)
		assert.NoError(t, err)

		userRead := usersRepo.Read(user.ID)
		assert.NotNil(t, userRead.ID)
		assert.Equal(t, "Super John", userRead.Name)
	})

	t.Run("TestDelete", func(t *testing.T) {
		// удаление пользователя
		usersRepo.Delete(user.ID)
		userRead := usersRepo.Read(user.ID)
		assert.Empty(t, userRead.ID)
	})

	t.Run("TestFind", func(t *testing.T) {
		// поиск всех пользователей
		usersRepo.Create(user)
		usersList := usersRepo.Find()
		assert.Len(t, usersList, 1)
	})
}
